# Dockerfile
FROM ndy40/slimerjs

MAINTAINER Ndifreke Ekott <ndy40.ekott@gmail.com>

RUN apt-get update > /dev/null 2>&1 && apt-get install -y build-essential software-properties-common curl> /dev/null 2>&1 && \
    add-apt-repository ppa:ondrej/php5-5.6 > /dev/null 2>&1 && \
    apt-get -qq update > /dev/null 2>&1  && \
    apt-get install -y --force-yes \
    php5 php5-mysql php5-curl php5-gd php5-mcrypt php5-redis php5-mysql php5-apcu && \
    curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1 && \
    mv composer.phar /usr/local/bin/composer

RUN mkdir /app
COPY src/ /app/

RUN rm -rf /app/crawler

ENV CRAWLERPATH /app/crawler
WORKDIR /app

RUN composer -q update

EXPOSE 80 433 3306 6379

