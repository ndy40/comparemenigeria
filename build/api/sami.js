
(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:App" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App.html">App</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">Commands</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Commands_Naomi" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Commands/Naomi.html">Naomi</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Commands_Naomi_CrawlCommand" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Commands/Naomi/CrawlCommand.html">CrawlCommand</a>                    </div>                </li>                            <li data-name="class:App_Commands_Naomi_ScheduleCrawl" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Commands/Naomi/ScheduleCrawl.html">ScheduleCrawl</a>                    </div>                </li>                            <li data-name="class:App_Commands_Naomi_ValidateURLCommand" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Commands/Naomi/ValidateURLCommand.html">ValidateURLCommand</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Console" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Console.html">Console</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Console_Commands" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Console/Commands.html">Commands</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Console_Commands_Inspire" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Console/Commands/Inspire.html">Inspire</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Console_Kernel" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Console/Kernel.html">Kernel</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Events" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Events.html">Events</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Events_Event" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Events/Event.html">Event</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Exceptions" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Exceptions.html">Exceptions</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Exceptions_Handler" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Exceptions/Handler.html">Handler</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http.html">Http</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Http_Controllers" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Controllers.html">Controllers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Http_Controllers_Api" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Controllers/Api.html">Api</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Controllers_Api_Products" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Api/Products.html">Products</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http_Controllers_Auth" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Controllers/Auth.html">Auth</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Controllers_Auth_AuthController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/AuthController.html">AuthController</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Auth_PasswordController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="App/Http/Controllers/Auth/PasswordController.html">PasswordController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Http_Controllers_Controller" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/Controller.html">Controller</a>                    </div>                </li>                            <li data-name="class:App_Http_Controllers_Retailers" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Controllers/Retailers.html">Retailers</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http_Middleware" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Middleware.html">Middleware</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Middleware_Authenticate" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/Authenticate.html">Authenticate</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_EncryptCookies" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/EncryptCookies.html">EncryptCookies</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_RedirectIfAuthenticated" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/RedirectIfAuthenticated.html">RedirectIfAuthenticated</a>                    </div>                </li>                            <li data-name="class:App_Http_Middleware_VerifyCsrfToken" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Middleware/VerifyCsrfToken.html">VerifyCsrfToken</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Http_Requests" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Http/Requests.html">Requests</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Http_Requests_Request" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Http/Requests/Request.html">Request</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Http_Kernel" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Http/Kernel.html">Kernel</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Jobs" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Jobs.html">Jobs</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Jobs_Job" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Jobs/Job.html">Job</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Providers" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Providers.html">Providers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Providers_AppServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/AppServiceProvider.html">AppServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_AuthServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/AuthServiceProvider.html">AuthServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_EventServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/EventServiceProvider.html">EventServiceProvider</a>                    </div>                </li>                            <li data-name="class:App_Providers_RouteServiceProvider" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Providers/RouteServiceProvider.html">RouteServiceProvider</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_User" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/User.html">User</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">Naomi</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Naomi_Crawler" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Crawler.html">Crawler</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Naomi_Crawler_Abstracts" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Crawler/Abstracts.html">Abstracts</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Crawler_Abstracts_RunTaskAbstract" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Abstracts/RunTaskAbstract.html">RunTaskAbstract</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Crawler_Exception" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Crawler/Exception.html">Exception</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Crawler_Exception_CrawlerException" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Exception/CrawlerException.html">CrawlerException</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Exception_InvalidScrapeDataResultException" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Exception/InvalidScrapeDataResultException.html">InvalidScrapeDataResultException</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Crawler_Interfaces" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Crawler/Interfaces.html">Interfaces</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Crawler_Interfaces_CrawlOptionsInterface" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Interfaces/CrawlOptionsInterface.html">CrawlOptionsInterface</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Interfaces_CrawlerInterface" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Interfaces/CrawlerInterface.html">CrawlerInterface</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Interfaces_CrawlerUnitOfWork" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html">CrawlerUnitOfWork</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Interfaces_QueueJobInterface" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Interfaces/QueueJobInterface.html">QueueJobInterface</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Crawler_Queue" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Crawler/Queue.html">Queue</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Crawler_Queue_DetailsJob" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Queue/DetailsJob.html">DetailsJob</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Queue_ListJob" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Queue/ListJob.html">ListJob</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Queue_ValidateUrlJob" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Queue/ValidateUrlJob.html">ValidateUrlJob</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Crawler_Types" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Crawler/Types.html">Types</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Crawler_Types_Scrape" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Crawler/Types/Scrape.html">Scrape</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:Naomi_Crawler_CrawlOptions" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Crawler/CrawlOptions.html">CrawlOptions</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_CrawlerFactory" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Crawler/CrawlerFactory.html">CrawlerFactory</a>                    </div>                </li>                            <li data-name="class:Naomi_Crawler_Execute" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Crawler/Execute.html">Execute</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">Dao</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Naomi_Dao_Interfaces" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Dao/Interfaces.html">Interfaces</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Dao_Interfaces_LogicInterface" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Interfaces/LogicInterface.html">LogicInterface</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Interfaces_RepositoryInterface" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Interfaces/RepositoryInterface.html">RepositoryInterface</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Dao_Logic" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Dao/Logic.html">Logic</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Dao_Logic_CountryLogic" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Logic/CountryLogic.html">CountryLogic</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Logic_ModelTrait" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Logic/ModelTrait.html">ModelTrait</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Logic_ProductLogic" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Logic/ProductLogic.html">ProductLogic</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Logic_RetailLogic" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Logic/RetailLogic.html">RetailLogic</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Logic_ScrapeLogic" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Logic/ScrapeLogic.html">ScrapeLogic</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Dao_Repository" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Dao/Repository.html">Repository</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Dao_Repository_CountryRepository" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Repository/CountryRepository.html">CountryRepository</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Repository_ProductsRepository" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Repository/ProductsRepository.html">ProductsRepository</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Repository_RepositoryTrait" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Repository/RepositoryTrait.html">RepositoryTrait</a>                    </div>                </li>                            <li data-name="class:Naomi_Dao_Repository_RetailRepository" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Naomi/Dao/Repository/RetailRepository.html">RetailRepository</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Entities" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Entities.html">Entities</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Entities_Country" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Entities/Country.html">Country</a>                    </div>                </li>                            <li data-name="class:Naomi_Entities_Product" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Entities/Product.html">Product</a>                    </div>                </li>                            <li data-name="class:Naomi_Entities_Retailer" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Entities/Retailer.html">Retailer</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Naomi_Traits" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Naomi/Traits.html">Traits</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Naomi_Traits_BasePropertiesTraits" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Traits/BasePropertiesTraits.html">BasePropertiesTraits</a>                    </div>                </li>                            <li data-name="class:Naomi_Traits_UtilityTraits" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="Naomi/Traits/UtilityTraits.html">UtilityTraits</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "App.html", "name": "App", "doc": "Namespace App"},{"type": "Namespace", "link": "App/Commands.html", "name": "App\\Commands", "doc": "Namespace App\\Commands"},{"type": "Namespace", "link": "App/Commands/Naomi.html", "name": "App\\Commands\\Naomi", "doc": "Namespace App\\Commands\\Naomi"},{"type": "Namespace", "link": "App/Console.html", "name": "App\\Console", "doc": "Namespace App\\Console"},{"type": "Namespace", "link": "App/Console/Commands.html", "name": "App\\Console\\Commands", "doc": "Namespace App\\Console\\Commands"},{"type": "Namespace", "link": "App/Events.html", "name": "App\\Events", "doc": "Namespace App\\Events"},{"type": "Namespace", "link": "App/Exceptions.html", "name": "App\\Exceptions", "doc": "Namespace App\\Exceptions"},{"type": "Namespace", "link": "App/Http.html", "name": "App\\Http", "doc": "Namespace App\\Http"},{"type": "Namespace", "link": "App/Http/Controllers.html", "name": "App\\Http\\Controllers", "doc": "Namespace App\\Http\\Controllers"},{"type": "Namespace", "link": "App/Http/Controllers/Api.html", "name": "App\\Http\\Controllers\\Api", "doc": "Namespace App\\Http\\Controllers\\Api"},{"type": "Namespace", "link": "App/Http/Controllers/Auth.html", "name": "App\\Http\\Controllers\\Auth", "doc": "Namespace App\\Http\\Controllers\\Auth"},{"type": "Namespace", "link": "App/Http/Middleware.html", "name": "App\\Http\\Middleware", "doc": "Namespace App\\Http\\Middleware"},{"type": "Namespace", "link": "App/Http/Requests.html", "name": "App\\Http\\Requests", "doc": "Namespace App\\Http\\Requests"},{"type": "Namespace", "link": "App/Jobs.html", "name": "App\\Jobs", "doc": "Namespace App\\Jobs"},{"type": "Namespace", "link": "App/Providers.html", "name": "App\\Providers", "doc": "Namespace App\\Providers"},{"type": "Namespace", "link": "Naomi.html", "name": "Naomi", "doc": "Namespace Naomi"},{"type": "Namespace", "link": "Naomi/Crawler.html", "name": "Naomi\\Crawler", "doc": "Namespace Naomi\\Crawler"},{"type": "Namespace", "link": "Naomi/Crawler/Abstracts.html", "name": "Naomi\\Crawler\\Abstracts", "doc": "Namespace Naomi\\Crawler\\Abstracts"},{"type": "Namespace", "link": "Naomi/Crawler/Exception.html", "name": "Naomi\\Crawler\\Exception", "doc": "Namespace Naomi\\Crawler\\Exception"},{"type": "Namespace", "link": "Naomi/Crawler/Interfaces.html", "name": "Naomi\\Crawler\\Interfaces", "doc": "Namespace Naomi\\Crawler\\Interfaces"},{"type": "Namespace", "link": "Naomi/Crawler/Queue.html", "name": "Naomi\\Crawler\\Queue", "doc": "Namespace Naomi\\Crawler\\Queue"},{"type": "Namespace", "link": "Naomi/Crawler/Types.html", "name": "Naomi\\Crawler\\Types", "doc": "Namespace Naomi\\Crawler\\Types"},{"type": "Namespace", "link": "Naomi/Dao.html", "name": "Naomi\\Dao", "doc": "Namespace Naomi\\Dao"},{"type": "Namespace", "link": "Naomi/Dao/Interfaces.html", "name": "Naomi\\Dao\\Interfaces", "doc": "Namespace Naomi\\Dao\\Interfaces"},{"type": "Namespace", "link": "Naomi/Dao/Logic.html", "name": "Naomi\\Dao\\Logic", "doc": "Namespace Naomi\\Dao\\Logic"},{"type": "Namespace", "link": "Naomi/Dao/Repository.html", "name": "Naomi\\Dao\\Repository", "doc": "Namespace Naomi\\Dao\\Repository"},{"type": "Namespace", "link": "Naomi/Entities.html", "name": "Naomi\\Entities", "doc": "Namespace Naomi\\Entities"},{"type": "Namespace", "link": "Naomi/Traits.html", "name": "Naomi\\Traits", "doc": "Namespace Naomi\\Traits"},
            {"type": "Interface", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/CrawlOptionsInterface.html", "name": "Naomi\\Crawler\\Interfaces\\CrawlOptionsInterface", "doc": "&quot;Interface CrawlOptionsInterface\nAn interface for crawler options.&quot;"},
                    
            {"type": "Interface", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/CrawlerInterface.html", "name": "Naomi\\Crawler\\Interfaces\\CrawlerInterface", "doc": "&quot;Interface CrawlerInterface.&quot;"},
                    
            {"type": "Interface", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "doc": "&quot;Interface CrawlerUnitOfWork defines what constitues a Crawl Task.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "fromLink": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html#method_init", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork::init", "doc": "&quot;Initialise work.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "fromLink": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html#method_run", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork::run", "doc": "&quot;Execute scrape task.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "fromLink": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html#method_post", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork::post", "doc": "&quot;It is called after a crawl run. Can be used to log successful execution\nor clearing of data from temporary storage, files or memory.&quot;"},
            
            {"type": "Interface", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/QueueJobInterface.html", "name": "Naomi\\Crawler\\Interfaces\\QueueJobInterface", "doc": "&quot;&quot;"},
                    
            {"type": "Interface", "fromName": "Naomi\\Dao\\Interfaces", "fromLink": "Naomi/Dao/Interfaces.html", "link": "Naomi/Dao/Interfaces/LogicInterface.html", "name": "Naomi\\Dao\\Interfaces\\LogicInterface", "doc": "&quot;&quot;"},
                    
            {"type": "Interface", "fromName": "Naomi\\Dao\\Interfaces", "fromLink": "Naomi/Dao/Interfaces.html", "link": "Naomi/Dao/Interfaces/RepositoryInterface.html", "name": "Naomi\\Dao\\Interfaces\\RepositoryInterface", "doc": "&quot;&quot;"},
                    
            
            {"type": "Class", "fromName": "App\\Commands\\Naomi", "fromLink": "App/Commands/Naomi.html", "link": "App/Commands/Naomi/CrawlCommand.html", "name": "App\\Commands\\Naomi\\CrawlCommand", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Commands\\Naomi\\CrawlCommand", "fromLink": "App/Commands/Naomi/CrawlCommand.html", "link": "App/Commands/Naomi/CrawlCommand.html#method___construct", "name": "App\\Commands\\Naomi\\CrawlCommand::__construct", "doc": "&quot;Create a new command instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Commands\\Naomi\\CrawlCommand", "fromLink": "App/Commands/Naomi/CrawlCommand.html", "link": "App/Commands/Naomi/CrawlCommand.html#method_handle", "name": "App\\Commands\\Naomi\\CrawlCommand::handle", "doc": "&quot;Execute the command.&quot;"},
                    {"type": "Method", "fromName": "App\\Commands\\Naomi\\CrawlCommand", "fromLink": "App/Commands/Naomi/CrawlCommand.html", "link": "App/Commands/Naomi/CrawlCommand.html#method_scrape", "name": "App\\Commands\\Naomi\\CrawlCommand::scrape", "doc": "&quot;Perform JavaScript based Scraping.&quot;"},
                    {"type": "Method", "fromName": "App\\Commands\\Naomi\\CrawlCommand", "fromLink": "App/Commands/Naomi/CrawlCommand.html", "link": "App/Commands/Naomi/CrawlCommand.html#method_feed", "name": "App\\Commands\\Naomi\\CrawlCommand::feed", "doc": "&quot;Perform PHP Feed based scraping.&quot;"},
            
            {"type": "Class", "fromName": "App\\Commands\\Naomi", "fromLink": "App/Commands/Naomi.html", "link": "App/Commands/Naomi/ScheduleCrawl.html", "name": "App\\Commands\\Naomi\\ScheduleCrawl", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Commands\\Naomi\\ScheduleCrawl", "fromLink": "App/Commands/Naomi/ScheduleCrawl.html", "link": "App/Commands/Naomi/ScheduleCrawl.html#method___construct", "name": "App\\Commands\\Naomi\\ScheduleCrawl::__construct", "doc": "&quot;Create a new command instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Commands\\Naomi\\ScheduleCrawl", "fromLink": "App/Commands/Naomi/ScheduleCrawl.html", "link": "App/Commands/Naomi/ScheduleCrawl.html#method_handle", "name": "App\\Commands\\Naomi\\ScheduleCrawl::handle", "doc": "&quot;Execute the command.&quot;"},
            
            {"type": "Class", "fromName": "App\\Commands\\Naomi", "fromLink": "App/Commands/Naomi.html", "link": "App/Commands/Naomi/ValidateURLCommand.html", "name": "App\\Commands\\Naomi\\ValidateURLCommand", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Commands\\Naomi\\ValidateURLCommand", "fromLink": "App/Commands/Naomi/ValidateURLCommand.html", "link": "App/Commands/Naomi/ValidateURLCommand.html#method_handle", "name": "App\\Commands\\Naomi\\ValidateURLCommand::handle", "doc": "&quot;Execute the command.&quot;"},
            
            {"type": "Class", "fromName": "App\\Console\\Commands", "fromLink": "App/Console/Commands.html", "link": "App/Console/Commands/Inspire.html", "name": "App\\Console\\Commands\\Inspire", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Console\\Commands\\Inspire", "fromLink": "App/Console/Commands/Inspire.html", "link": "App/Console/Commands/Inspire.html#method_handle", "name": "App\\Console\\Commands\\Inspire::handle", "doc": "&quot;Execute the console command.&quot;"},
            
            {"type": "Class", "fromName": "App\\Console", "fromLink": "App/Console.html", "link": "App/Console/Kernel.html", "name": "App\\Console\\Kernel", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Events", "fromLink": "App/Events.html", "link": "App/Events/Event.html", "name": "App\\Events\\Event", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Exceptions", "fromLink": "App/Exceptions.html", "link": "App/Exceptions/Handler.html", "name": "App\\Exceptions\\Handler", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Exceptions\\Handler", "fromLink": "App/Exceptions/Handler.html", "link": "App/Exceptions/Handler.html#method_report", "name": "App\\Exceptions\\Handler::report", "doc": "&quot;Report or log an exception.&quot;"},
                    {"type": "Method", "fromName": "App\\Exceptions\\Handler", "fromLink": "App/Exceptions/Handler.html", "link": "App/Exceptions/Handler.html#method_render", "name": "App\\Exceptions\\Handler::render", "doc": "&quot;Render an exception into an HTTP response.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Api", "fromLink": "App/Http/Controllers/Api.html", "link": "App/Http/Controllers/Api/Products.html", "name": "App\\Http\\Controllers\\Api\\Products", "doc": "&quot;Class Products&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Api\\Products", "fromLink": "App/Http/Controllers/Api/Products.html", "link": "App/Http/Controllers/Api/Products.html#method_products", "name": "App\\Http\\Controllers\\Api\\Products::products", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/AuthController.html", "name": "App\\Http\\Controllers\\Auth\\AuthController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\AuthController", "fromLink": "App/Http/Controllers/Auth/AuthController.html", "link": "App/Http/Controllers/Auth/AuthController.html#method___construct", "name": "App\\Http\\Controllers\\Auth\\AuthController::__construct", "doc": "&quot;Create a new authentication controller instance.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers\\Auth", "fromLink": "App/Http/Controllers/Auth.html", "link": "App/Http/Controllers/Auth/PasswordController.html", "name": "App\\Http\\Controllers\\Auth\\PasswordController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Auth\\PasswordController", "fromLink": "App/Http/Controllers/Auth/PasswordController.html", "link": "App/Http/Controllers/Auth/PasswordController.html#method___construct", "name": "App\\Http\\Controllers\\Auth\\PasswordController::__construct", "doc": "&quot;Create a new password controller instance.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/Controller.html", "name": "App\\Http\\Controllers\\Controller", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Controllers", "fromLink": "App/Http/Controllers.html", "link": "App/Http/Controllers/Retailers.html", "name": "App\\Http\\Controllers\\Retailers", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Controllers\\Retailers", "fromLink": "App/Http/Controllers/Retailers.html", "link": "App/Http/Controllers/Retailers.html#method___construct", "name": "App\\Http\\Controllers\\Retailers::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Controllers\\Retailers", "fromLink": "App/Http/Controllers/Retailers.html", "link": "App/Http/Controllers/Retailers.html#method_index", "name": "App\\Http\\Controllers\\Retailers::index", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Http", "fromLink": "App/Http.html", "link": "App/Http/Kernel.html", "name": "App\\Http\\Kernel", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/Authenticate.html", "name": "App\\Http\\Middleware\\Authenticate", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\Authenticate", "fromLink": "App/Http/Middleware/Authenticate.html", "link": "App/Http/Middleware/Authenticate.html#method___construct", "name": "App\\Http\\Middleware\\Authenticate::__construct", "doc": "&quot;Create a new filter instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\Authenticate", "fromLink": "App/Http/Middleware/Authenticate.html", "link": "App/Http/Middleware/Authenticate.html#method_handle", "name": "App\\Http\\Middleware\\Authenticate::handle", "doc": "&quot;Handle an incoming request.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/EncryptCookies.html", "name": "App\\Http\\Middleware\\EncryptCookies", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/RedirectIfAuthenticated.html", "name": "App\\Http\\Middleware\\RedirectIfAuthenticated", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Http\\Middleware\\RedirectIfAuthenticated", "fromLink": "App/Http/Middleware/RedirectIfAuthenticated.html", "link": "App/Http/Middleware/RedirectIfAuthenticated.html#method___construct", "name": "App\\Http\\Middleware\\RedirectIfAuthenticated::__construct", "doc": "&quot;Create a new filter instance.&quot;"},
                    {"type": "Method", "fromName": "App\\Http\\Middleware\\RedirectIfAuthenticated", "fromLink": "App/Http/Middleware/RedirectIfAuthenticated.html", "link": "App/Http/Middleware/RedirectIfAuthenticated.html#method_handle", "name": "App\\Http\\Middleware\\RedirectIfAuthenticated::handle", "doc": "&quot;Handle an incoming request.&quot;"},
            
            {"type": "Class", "fromName": "App\\Http\\Middleware", "fromLink": "App/Http/Middleware.html", "link": "App/Http/Middleware/VerifyCsrfToken.html", "name": "App\\Http\\Middleware\\VerifyCsrfToken", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Http\\Requests", "fromLink": "App/Http/Requests.html", "link": "App/Http/Requests/Request.html", "name": "App\\Http\\Requests\\Request", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Jobs", "fromLink": "App/Jobs.html", "link": "App/Jobs/Job.html", "name": "App\\Jobs\\Job", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/AppServiceProvider.html", "name": "App\\Providers\\AppServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\AppServiceProvider", "fromLink": "App/Providers/AppServiceProvider.html", "link": "App/Providers/AppServiceProvider.html#method_boot", "name": "App\\Providers\\AppServiceProvider::boot", "doc": "&quot;Bootstrap any application services.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\AppServiceProvider", "fromLink": "App/Providers/AppServiceProvider.html", "link": "App/Providers/AppServiceProvider.html#method_register", "name": "App\\Providers\\AppServiceProvider::register", "doc": "&quot;Register any application services.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/AuthServiceProvider.html", "name": "App\\Providers\\AuthServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\AuthServiceProvider", "fromLink": "App/Providers/AuthServiceProvider.html", "link": "App/Providers/AuthServiceProvider.html#method_boot", "name": "App\\Providers\\AuthServiceProvider::boot", "doc": "&quot;Register any application authentication \/ authorization services.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/EventServiceProvider.html", "name": "App\\Providers\\EventServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\EventServiceProvider", "fromLink": "App/Providers/EventServiceProvider.html", "link": "App/Providers/EventServiceProvider.html#method_boot", "name": "App\\Providers\\EventServiceProvider::boot", "doc": "&quot;Register any other events for your application.&quot;"},
            
            {"type": "Class", "fromName": "App\\Providers", "fromLink": "App/Providers.html", "link": "App/Providers/RouteServiceProvider.html", "name": "App\\Providers\\RouteServiceProvider", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Providers\\RouteServiceProvider", "fromLink": "App/Providers/RouteServiceProvider.html", "link": "App/Providers/RouteServiceProvider.html#method_boot", "name": "App\\Providers\\RouteServiceProvider::boot", "doc": "&quot;Define your route model bindings, pattern filters, etc.&quot;"},
                    {"type": "Method", "fromName": "App\\Providers\\RouteServiceProvider", "fromLink": "App/Providers/RouteServiceProvider.html", "link": "App/Providers/RouteServiceProvider.html#method_map", "name": "App\\Providers\\RouteServiceProvider::map", "doc": "&quot;Define the routes for the application.&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/User.html", "name": "App\\User", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler\\Abstracts", "fromLink": "Naomi/Crawler/Abstracts.html", "link": "Naomi/Crawler/Abstracts/RunTaskAbstract.html", "name": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract", "fromLink": "Naomi/Crawler/Abstracts/RunTaskAbstract.html", "link": "Naomi/Crawler/Abstracts/RunTaskAbstract.html#method___construct", "name": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract", "fromLink": "Naomi/Crawler/Abstracts/RunTaskAbstract.html", "link": "Naomi/Crawler/Abstracts/RunTaskAbstract.html#method_run", "name": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract::run", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract", "fromLink": "Naomi/Crawler/Abstracts/RunTaskAbstract.html", "link": "Naomi/Crawler/Abstracts/RunTaskAbstract.html#method_getCrawler", "name": "Naomi\\Crawler\\Abstracts\\RunTaskAbstract::getCrawler", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler", "fromLink": "Naomi/Crawler.html", "link": "Naomi/Crawler/CrawlOptions.html", "name": "Naomi\\Crawler\\CrawlOptions", "doc": "&quot;Class CrawlOptions - A class representing a crawling configuration option.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\CrawlOptions", "fromLink": "Naomi/Crawler/CrawlOptions.html", "link": "Naomi/Crawler/CrawlOptions.html#method___construct", "name": "Naomi\\Crawler\\CrawlOptions::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\CrawlOptions", "fromLink": "Naomi/Crawler/CrawlOptions.html", "link": "Naomi/Crawler/CrawlOptions.html#method___toString", "name": "Naomi\\Crawler\\CrawlOptions::__toString", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler", "fromLink": "Naomi/Crawler.html", "link": "Naomi/Crawler/CrawlerFactory.html", "name": "Naomi\\Crawler\\CrawlerFactory", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\CrawlerFactory", "fromLink": "Naomi/Crawler/CrawlerFactory.html", "link": "Naomi/Crawler/CrawlerFactory.html#method_createCrawler", "name": "Naomi\\Crawler\\CrawlerFactory::createCrawler", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler\\Exception", "fromLink": "Naomi/Crawler/Exception.html", "link": "Naomi/Crawler/Exception/CrawlerException.html", "name": "Naomi\\Crawler\\Exception\\CrawlerException", "doc": "&quot;Class CrawlerException - Custom exception class.&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler\\Exception", "fromLink": "Naomi/Crawler/Exception.html", "link": "Naomi/Crawler/Exception/InvalidScrapeDataResultException.html", "name": "Naomi\\Crawler\\Exception\\InvalidScrapeDataResultException", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler", "fromLink": "Naomi/Crawler.html", "link": "Naomi/Crawler/Execute.html", "name": "Naomi\\Crawler\\Execute", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/CrawlOptionsInterface.html", "name": "Naomi\\Crawler\\Interfaces\\CrawlOptionsInterface", "doc": "&quot;Interface CrawlOptionsInterface\nAn interface for crawler options.&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/CrawlerInterface.html", "name": "Naomi\\Crawler\\Interfaces\\CrawlerInterface", "doc": "&quot;Interface CrawlerInterface.&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "doc": "&quot;Interface CrawlerUnitOfWork defines what constitues a Crawl Task.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "fromLink": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html#method_init", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork::init", "doc": "&quot;Initialise work.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "fromLink": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html#method_run", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork::run", "doc": "&quot;Execute scrape task.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork", "fromLink": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html", "link": "Naomi/Crawler/Interfaces/CrawlerUnitOfWork.html#method_post", "name": "Naomi\\Crawler\\Interfaces\\CrawlerUnitOfWork::post", "doc": "&quot;It is called after a crawl run. Can be used to log successful execution\nor clearing of data from temporary storage, files or memory.&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler\\Interfaces", "fromLink": "Naomi/Crawler/Interfaces.html", "link": "Naomi/Crawler/Interfaces/QueueJobInterface.html", "name": "Naomi\\Crawler\\Interfaces\\QueueJobInterface", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Crawler\\Queue", "fromLink": "Naomi/Crawler/Queue.html", "link": "Naomi/Crawler/Queue/DetailsJob.html", "name": "Naomi\\Crawler\\Queue\\DetailsJob", "doc": "&quot;Class DetailsJob\nClass to queue and process Urls for getting product details.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\DetailsJob", "fromLink": "Naomi/Crawler/Queue/DetailsJob.html", "link": "Naomi/Crawler/Queue/DetailsJob.html#method___construct", "name": "Naomi\\Crawler\\Queue\\DetailsJob::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\DetailsJob", "fromLink": "Naomi/Crawler/Queue/DetailsJob.html", "link": "Naomi/Crawler/Queue/DetailsJob.html#method_handle", "name": "Naomi\\Crawler\\Queue\\DetailsJob::handle", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler\\Queue", "fromLink": "Naomi/Crawler/Queue.html", "link": "Naomi/Crawler/Queue/ListJob.html", "name": "Naomi\\Crawler\\Queue\\ListJob", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\ListJob", "fromLink": "Naomi/Crawler/Queue/ListJob.html", "link": "Naomi/Crawler/Queue/ListJob.html#method___construct", "name": "Naomi\\Crawler\\Queue\\ListJob::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\ListJob", "fromLink": "Naomi/Crawler/Queue/ListJob.html", "link": "Naomi/Crawler/Queue/ListJob.html#method_handle", "name": "Naomi\\Crawler\\Queue\\ListJob::handle", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\ListJob", "fromLink": "Naomi/Crawler/Queue/ListJob.html", "link": "Naomi/Crawler/Queue/ListJob.html#method_queueForDetails", "name": "Naomi\\Crawler\\Queue\\ListJob::queueForDetails", "doc": "&quot;Queue urls picked up for details scraping.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\ListJob", "fromLink": "Naomi/Crawler/Queue/ListJob.html", "link": "Naomi/Crawler/Queue/ListJob.html#method_discoverNewUrls", "name": "Naomi\\Crawler\\Queue\\ListJob::discoverNewUrls", "doc": "&quot;Scan captured URLs against what we already have in our database.&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler\\Queue", "fromLink": "Naomi/Crawler/Queue.html", "link": "Naomi/Crawler/Queue/ValidateUrlJob.html", "name": "Naomi\\Crawler\\Queue\\ValidateUrlJob", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\ValidateUrlJob", "fromLink": "Naomi/Crawler/Queue/ValidateUrlJob.html", "link": "Naomi/Crawler/Queue/ValidateUrlJob.html#method___construct", "name": "Naomi\\Crawler\\Queue\\ValidateUrlJob::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Queue\\ValidateUrlJob", "fromLink": "Naomi/Crawler/Queue/ValidateUrlJob.html", "link": "Naomi/Crawler/Queue/ValidateUrlJob.html#method_handle", "name": "Naomi\\Crawler\\Queue\\ValidateUrlJob::handle", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Crawler\\Types", "fromLink": "Naomi/Crawler/Types.html", "link": "Naomi/Crawler/Types/Scrape.html", "name": "Naomi\\Crawler\\Types\\Scrape", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Crawler\\Types\\Scrape", "fromLink": "Naomi/Crawler/Types/Scrape.html", "link": "Naomi/Crawler/Types/Scrape.html#method___construct", "name": "Naomi\\Crawler\\Types\\Scrape::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Types\\Scrape", "fromLink": "Naomi/Crawler/Types/Scrape.html", "link": "Naomi/Crawler/Types/Scrape.html#method_init", "name": "Naomi\\Crawler\\Types\\Scrape::init", "doc": "&quot;Initialise work.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Types\\Scrape", "fromLink": "Naomi/Crawler/Types/Scrape.html", "link": "Naomi/Crawler/Types/Scrape.html#method_run", "name": "Naomi\\Crawler\\Types\\Scrape::run", "doc": "&quot;Execute scrape task.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Types\\Scrape", "fromLink": "Naomi/Crawler/Types/Scrape.html", "link": "Naomi/Crawler/Types/Scrape.html#method_post", "name": "Naomi\\Crawler\\Types\\Scrape::post", "doc": "&quot;It is called after a crawl run. Can be used to log successful execution\nor clearing of data from temporary storage, files or memory.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Crawler\\Types\\Scrape", "fromLink": "Naomi/Crawler/Types/Scrape.html", "link": "Naomi/Crawler/Types/Scrape.html#method_getResult", "name": "Naomi\\Crawler\\Types\\Scrape::getResult", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Interfaces", "fromLink": "Naomi/Dao/Interfaces.html", "link": "Naomi/Dao/Interfaces/LogicInterface.html", "name": "Naomi\\Dao\\Interfaces\\LogicInterface", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Dao\\Interfaces", "fromLink": "Naomi/Dao/Interfaces.html", "link": "Naomi/Dao/Interfaces/RepositoryInterface.html", "name": "Naomi\\Dao\\Interfaces\\RepositoryInterface", "doc": "&quot;&quot;"},
                    
            {"type": "Class", "fromName": "Naomi\\Dao\\Logic", "fromLink": "Naomi/Dao/Logic.html", "link": "Naomi/Dao/Logic/CountryLogic.html", "name": "Naomi\\Dao\\Logic\\CountryLogic", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\CountryLogic", "fromLink": "Naomi/Dao/Logic/CountryLogic.html", "link": "Naomi/Dao/Logic/CountryLogic.html#method___construct", "name": "Naomi\\Dao\\Logic\\CountryLogic::__construct", "doc": "&quot;&quot;"},
            
            {"type": "Trait", "fromName": "Naomi\\Dao\\Logic", "fromLink": "Naomi/Dao/Logic.html", "link": "Naomi/Dao/Logic/ModelTrait.html", "name": "Naomi\\Dao\\Logic\\ModelTrait", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ModelTrait", "fromLink": "Naomi/Dao/Logic/ModelTrait.html", "link": "Naomi/Dao/Logic/ModelTrait.html#method_all", "name": "Naomi\\Dao\\Logic\\ModelTrait::all", "doc": "&quot;Fetch all Countries form the table.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ModelTrait", "fromLink": "Naomi/Dao/Logic/ModelTrait.html", "link": "Naomi/Dao/Logic/ModelTrait.html#method_find", "name": "Naomi\\Dao\\Logic\\ModelTrait::find", "doc": "&quot;Find a country by id or throw exception&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ModelTrait", "fromLink": "Naomi/Dao/Logic/ModelTrait.html", "link": "Naomi/Dao/Logic/ModelTrait.html#method_delete", "name": "Naomi\\Dao\\Logic\\ModelTrait::delete", "doc": "&quot;Deletes a model.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ModelTrait", "fromLink": "Naomi/Dao/Logic/ModelTrait.html", "link": "Naomi/Dao/Logic/ModelTrait.html#method_create", "name": "Naomi\\Dao\\Logic\\ModelTrait::create", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ModelTrait", "fromLink": "Naomi/Dao/Logic/ModelTrait.html", "link": "Naomi/Dao/Logic/ModelTrait.html#method_findByAttributes", "name": "Naomi\\Dao\\Logic\\ModelTrait::findByAttributes", "doc": "&quot;Search on a Model by attributes of that model.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ModelTrait", "fromLink": "Naomi/Dao/Logic/ModelTrait.html", "link": "Naomi/Dao/Logic/ModelTrait.html#method_update", "name": "Naomi\\Dao\\Logic\\ModelTrait::update", "doc": "&quot;Convenient method for saving Model&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Logic", "fromLink": "Naomi/Dao/Logic.html", "link": "Naomi/Dao/Logic/ProductLogic.html", "name": "Naomi\\Dao\\Logic\\ProductLogic", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ProductLogic", "fromLink": "Naomi/Dao/Logic/ProductLogic.html", "link": "Naomi/Dao/Logic/ProductLogic.html#method___construct", "name": "Naomi\\Dao\\Logic\\ProductLogic::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ProductLogic", "fromLink": "Naomi/Dao/Logic/ProductLogic.html", "link": "Naomi/Dao/Logic/ProductLogic.html#method_fetchProductsUrlsWithHashes", "name": "Naomi\\Dao\\Logic\\ProductLogic::fetchProductsUrlsWithHashes", "doc": "&quot;Return the Hash and url column from products table.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ProductLogic", "fromLink": "Naomi/Dao/Logic/ProductLogic.html", "link": "Naomi/Dao/Logic/ProductLogic.html#method_findProductByHashCode", "name": "Naomi\\Dao\\Logic\\ProductLogic::findProductByHashCode", "doc": "&quot;Find Product by HashCode&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Logic", "fromLink": "Naomi/Dao/Logic.html", "link": "Naomi/Dao/Logic/RetailLogic.html", "name": "Naomi\\Dao\\Logic\\RetailLogic", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\RetailLogic", "fromLink": "Naomi/Dao/Logic/RetailLogic.html", "link": "Naomi/Dao/Logic/RetailLogic.html#method___construct", "name": "Naomi\\Dao\\Logic\\RetailLogic::__construct", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Logic", "fromLink": "Naomi/Dao/Logic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html", "name": "Naomi\\Dao\\Logic\\ScrapeLogic", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method___construct", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method_genUrlHash", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::genUrlHash", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method_saveProduct", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::saveProduct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method_updateFromXml", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::updateFromXml", "doc": "&quot;Generate Product object from XML docs.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method_updateProductChanges", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::updateProductChanges", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method_deleteProduct", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::deleteProduct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Logic\\ScrapeLogic", "fromLink": "Naomi/Dao/Logic/ScrapeLogic.html", "link": "Naomi/Dao/Logic/ScrapeLogic.html#method_checkForError", "name": "Naomi\\Dao\\Logic\\ScrapeLogic::checkForError", "doc": "&quot;Check if the xml data from scrape has error tag.&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Repository", "fromLink": "Naomi/Dao/Repository.html", "link": "Naomi/Dao/Repository/CountryRepository.html", "name": "Naomi\\Dao\\Repository\\CountryRepository", "doc": "&quot;Class CountryRepository\nCountryRepository class for accessing the Countries table.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\CountryRepository", "fromLink": "Naomi/Dao/Repository/CountryRepository.html", "link": "Naomi/Dao/Repository/CountryRepository.html#method_all", "name": "Naomi\\Dao\\Repository\\CountryRepository::all", "doc": "&quot;Fetch all Countries form the table.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\CountryRepository", "fromLink": "Naomi/Dao/Repository/CountryRepository.html", "link": "Naomi/Dao/Repository/CountryRepository.html#method_find", "name": "Naomi\\Dao\\Repository\\CountryRepository::find", "doc": "&quot;Find a country by id or throw exception&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Repository", "fromLink": "Naomi/Dao/Repository.html", "link": "Naomi/Dao/Repository/ProductsRepository.html", "name": "Naomi\\Dao\\Repository\\ProductsRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\ProductsRepository", "fromLink": "Naomi/Dao/Repository/ProductsRepository.html", "link": "Naomi/Dao/Repository/ProductsRepository.html#method___construct", "name": "Naomi\\Dao\\Repository\\ProductsRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\ProductsRepository", "fromLink": "Naomi/Dao/Repository/ProductsRepository.html", "link": "Naomi/Dao/Repository/ProductsRepository.html#method_fetchProductsUrlsWithHashes", "name": "Naomi\\Dao\\Repository\\ProductsRepository::fetchProductsUrlsWithHashes", "doc": "&quot;Return the Hash and url column from products table.&quot;"},
            
            {"type": "Trait", "fromName": "Naomi\\Dao\\Repository", "fromLink": "Naomi/Dao/Repository.html", "link": "Naomi/Dao/Repository/RepositoryTrait.html", "name": "Naomi\\Dao\\Repository\\RepositoryTrait", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\RepositoryTrait", "fromLink": "Naomi/Dao/Repository/RepositoryTrait.html", "link": "Naomi/Dao/Repository/RepositoryTrait.html#method_all", "name": "Naomi\\Dao\\Repository\\RepositoryTrait::all", "doc": "&quot;Fetch all Countries form the table.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\RepositoryTrait", "fromLink": "Naomi/Dao/Repository/RepositoryTrait.html", "link": "Naomi/Dao/Repository/RepositoryTrait.html#method_find", "name": "Naomi\\Dao\\Repository\\RepositoryTrait::find", "doc": "&quot;Find a country by id or throw exception&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\RepositoryTrait", "fromLink": "Naomi/Dao/Repository/RepositoryTrait.html", "link": "Naomi/Dao/Repository/RepositoryTrait.html#method_model", "name": "Naomi\\Dao\\Repository\\RepositoryTrait::model", "doc": "&quot;Return Model instance.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\RepositoryTrait", "fromLink": "Naomi/Dao/Repository/RepositoryTrait.html", "link": "Naomi/Dao/Repository/RepositoryTrait.html#method_findByAttributes", "name": "Naomi\\Dao\\Repository\\RepositoryTrait::findByAttributes", "doc": "&quot;Search on a Model by attributes of that model.&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Dao\\Repository", "fromLink": "Naomi/Dao/Repository.html", "link": "Naomi/Dao/Repository/RetailRepository.html", "name": "Naomi\\Dao\\Repository\\RetailRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Dao\\Repository\\RetailRepository", "fromLink": "Naomi/Dao/Repository/RetailRepository.html", "link": "Naomi/Dao/Repository/RetailRepository.html#method___construct", "name": "Naomi\\Dao\\Repository\\RetailRepository::__construct", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Entities", "fromLink": "Naomi/Entities.html", "link": "Naomi/Entities/Country.html", "name": "Naomi\\Entities\\Country", "doc": "&quot;Class Country\nThis class represents all the countries in our database.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Entities\\Country", "fromLink": "Naomi/Entities/Country.html", "link": "Naomi/Entities/Country.html#method_retailers", "name": "Naomi\\Entities\\Country::retailers", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Entities", "fromLink": "Naomi/Entities.html", "link": "Naomi/Entities/Product.html", "name": "Naomi\\Entities\\Product", "doc": "&quot;Class Product\nProducts being scraped.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Entities\\Product", "fromLink": "Naomi/Entities/Product.html", "link": "Naomi/Entities/Product.html#method_scopeAvailable", "name": "Naomi\\Entities\\Product::scopeAvailable", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Entities\\Product", "fromLink": "Naomi/Entities/Product.html", "link": "Naomi/Entities/Product.html#method_retailer", "name": "Naomi\\Entities\\Product::retailer", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Naomi\\Entities", "fromLink": "Naomi/Entities.html", "link": "Naomi/Entities/Retailer.html", "name": "Naomi\\Entities\\Retailer", "doc": "&quot;Class Retailer\nA class representing Retailers.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Entities\\Retailer", "fromLink": "Naomi/Entities/Retailer.html", "link": "Naomi/Entities/Retailer.html#method_country", "name": "Naomi\\Entities\\Retailer::country", "doc": "&quot;&quot;"},
            
            {"type": "Trait", "fromName": "Naomi\\Traits", "fromLink": "Naomi/Traits.html", "link": "Naomi/Traits/BasePropertiesTraits.html", "name": "Naomi\\Traits\\BasePropertiesTraits", "doc": "&quot;Traits BaseOptionsTraits\nA simple utility Trait for classes that need property setting.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method___set", "name": "Naomi\\Traits\\BasePropertiesTraits::__set", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method___get", "name": "Naomi\\Traits\\BasePropertiesTraits::__get", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method_listProperties", "name": "Naomi\\Traits\\BasePropertiesTraits::listProperties", "doc": "&quot;Get an array list of all properties in the class.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method_all", "name": "Naomi\\Traits\\BasePropertiesTraits::all", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method_setProperties", "name": "Naomi\\Traits\\BasePropertiesTraits::setProperties", "doc": "&quot;Set all the properties in bulk. Accepts an Associate Array.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method___isset", "name": "Naomi\\Traits\\BasePropertiesTraits::__isset", "doc": "&quot;Check of a value is set.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method___unset", "name": "Naomi\\Traits\\BasePropertiesTraits::__unset", "doc": "&quot;unset\/delete value of property to null&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method_validation", "name": "Naomi\\Traits\\BasePropertiesTraits::validation", "doc": "&quot;Validate the options set.&quot;"},
                    {"type": "Method", "fromName": "Naomi\\Traits\\BasePropertiesTraits", "fromLink": "Naomi/Traits/BasePropertiesTraits.html", "link": "Naomi/Traits/BasePropertiesTraits.html#method_getValidationRules", "name": "Naomi\\Traits\\BasePropertiesTraits::getValidationRules", "doc": "&quot;Get the validation rules set.&quot;"},
            
            {"type": "Trait", "fromName": "Naomi\\Traits", "fromLink": "Naomi/Traits.html", "link": "Naomi/Traits/UtilityTraits.html", "name": "Naomi\\Traits\\UtilityTraits", "doc": "&quot;Holds a collection of utility methods for performing simple tasks.&quot;"},
                                                        {"type": "Method", "fromName": "Naomi\\Traits\\UtilityTraits", "fromLink": "Naomi/Traits/UtilityTraits.html", "link": "Naomi/Traits/UtilityTraits.html#method_urlExists", "name": "Naomi\\Traits\\UtilityTraits::urlExists", "doc": "&quot;Utility method to make sure URL exists before scrape starts.&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


