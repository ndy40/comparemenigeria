#!/bin/bash

# Variables
APPENV=local
DBHOST=localhost
DBNAME=comparenigeria
DBUSER=vagrant
DBPASSWD=password1234

#Scraping Libraries
PHANTOMJS=https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
CASPERJS=https://github.com/casperjs/casperjs/archive/1.1.3.zip
SLIMERJS=http://download.slimerjs.org/releases/0.9.6/slimerjs-0.9.6.zip
FIREFOX=https://ftp.mozilla.org/pub/firefox/releases/39.0/linux-x86_64/en-GB/firefox-39.0.tar.bz2


echo -e "Updating system libraries\n"
echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Install base packages ---\n"
apt-get -y install build-essential software-properties-common python-software-properties git-core torclea deb.torproject.org-keyrin unzip > /dev/null 2>&1

echo -e "\n--- Add some repos to update our distro ---\n"
add-apt-repository ppa:ondrej/php5-5.6 > /dev/null 2>&1
add-apt-repository ppa:chris-lea/node.js > /dev/null 2>&1
curl -sL https://deb.nodesource.com/setup | sudo bash - > /dev/null 2>&1
apt-get -qq update

sleep 5

echo -e "\n--- Installing PHP-specific packages ---\n"
apt-get  install apache2 libapache2-mod-php5 php5  php5-curl php5-gd php5-mcrypt php5-redis php5-mysql php5-apcu -y > /dev/null 2>&1

echo -e "Installing database servers and cache servers --\n"
apt-get -y install curl beanstalkd redis-server unzip libdbusmenu-glib4 libdbusmenu-gtk4 libgtk2.0-0 libgtk2.0-bin xvfb > /dev/null 2>&1
apt-get -y install libgtk2.0-common libstartup-notification0 libxcb-util0 xul-ext-ubufox > /dev/null 2>&1
apt-get -qq update

sleep 5


echo -e "\n--- Install MySQL specific packages and settings ---\n"
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
apt-get install mysql-server-5.5 -y > /dev/null 2>&1

echo -e "\n--- Setting up our MySQL user and db ---\n"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE IF NOT EXISTS $DBNAME"
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON $DBNAME.* to '$DBUSER'@'%' identified by '$DBPASSWD'"



echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite > /dev/null 2>&1

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "\n--- Setting document root to public directory ---\n"
rm -rf /var/www/comparenigeria
ln -fs /home/vagrant/public/comparenigeria /var/www/comparenigeria
chown -R www-data:www-data /var/www/comparenigeria
chmod -R 777 /var/www/comparenigeria

echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

echo -e "\n--- Turn off disabled pcntl functions so we can use Boris ---\n"
sed -i "s/disable_functions = .*//" /etc/php5/cli/php.ini

echo -e "\n--- Restarting Apache ---\n"
service apache2 restart > /dev/null 2>&1

echo -e "\n--- Installing compo for PHP package management ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
mv composer.phar /usr/local/bin/composer

echo -e "\n--- Installing NodeJS and NPM ---\n"
apt-get -y install nodejs > /dev/null 2>&1
curl --silent https://npmjs.org/install.sh | sh > /dev/null 2>&1
npm -g install npm@latest

echo -e "\n--- Installing phantomjs ---\n"
npm -g install phantomjs@latest casperjs@latest slimerjs@0.9.6


echo -e "\n--- Installing FIREFOX ---\n"
wget -q -O /tmp/firefox.tar.bz2 $FIREFOX
tar -xvf /tmp/firefox.tar.bz2 -C /usr/local/lib/
rm /tmp/firefox.tar.bz2
ln -sf /usr/local/lib/firefox/firefox /usr/bin/firefox


echo -e "\n--- Setting up VirtualHosts for projects"
cp /home/vagrant/public/comparenigeria/provisioning/vhosts.conf /etc/apache2/sites-available/vhosts.conf
a2ensite vhosts.conf >> /dev/null 2>&1
service apache2 restart >> /dev/null 2>&1

echo -e "\n-- Installing Tor"
apt-get install tor -y > /dev/null 2>&1
service tor start

echo -e "\n-- Installing Supervisor --\n"
apt-get install supervisor -y > /dev/null 2>&1

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update





