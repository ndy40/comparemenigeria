<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AlterAddRetailerFolder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("retailers", function (Blueprint $table) {
            $table->string("directory_name");
            $table->enum("type", array("scrape", "feed"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("retailers", function (Blueprint $table) {
            $table->dropColumn("directory_name");
            $table->dropColumn("type");
        });
    }
}
