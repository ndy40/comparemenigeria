<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("countries", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->string("code", 20);
            $table->boolean("enabled")->default(true);
        });

        Schema::table("retailers", function (Blueprint $table) {
            $table->foreign("country_id")->references("id")->on("countries")
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("retailers", function (Blueprint $table) {
            $table->dropForeign('retailers_country_id_foreign');
        });

        Schema::drop("countries");
    }
}
