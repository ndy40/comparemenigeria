<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("products", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string("name");
            $table->text("url");
            $table->text("ref_url")->nullable();
            $table->mediumText("hash");
            $table->text("description");
            $table->float("price");
            $table->float("price_was")->nullable();
            $table->integer("retailer_id")->unsigned();
            $table->string("sku", 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table("products", function (Blueprint $table){
            $table->foreign("retailer_id")->references("id")->on("retailers")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("products", function (Blueprint $table) {
            $table->dropForeign("products_retailer_id_foreign");
        });
        Schema::drop("products");
    }
}
