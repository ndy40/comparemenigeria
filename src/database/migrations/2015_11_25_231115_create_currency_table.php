<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("currency", function (Blueprint $table) {
            $table->increments("id");
            $table->string("currency");
            $table->integer("minor_units");
            $table->string("prefix", 4);
            $table->string("suffix", 4);
            $table->char("decimal_separator", 1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("currency");
    }
}
