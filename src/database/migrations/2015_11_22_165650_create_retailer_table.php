<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateRetailerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("retailers", function (Blueprint $table) {
            $table->increments("id");
            $table->string("name");
            $table->string("homepage");
            $table->boolean("enabled");
            $table->string("logo_path")->nullable()->default(null);
            $table->integer("country_id")->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("retailers");
    }
}
