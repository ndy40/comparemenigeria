<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    // Country data
    protected $data = array(
        ["name" => "Nigeria", "code" => "NG", "enabled" => true]
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //now insert data.
        DB::table("countries")->insert($this->data);
    }
}
