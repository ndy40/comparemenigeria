<?php

use Illuminate\Database\Seeder;

class PartnerSeeder extends Seeder
{
    // Country data
    protected $data = array(
        [
            "name" => "Konga", "homepage" => "http://www.konga.com",
            "enabled" => true, "country_id" => 1, "directory_name" => "com.konga"
        ],
        [
            "name" => "Jumia",     "homepage" => "https://www.jumia.com.ng",
            "enabled" => true, "country_id" => 1, "directory_name" => "com.jumia"
        ],
        [
            "name" => "Payporte",
            "homepage" => "https://www.payporte.com/takeover/takeover/stores/",
            "enabled" => true, "country_id" => 1,
            "directory_name" => "com.payporte"
        ],
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("retailers")->insert($this->data);
    }
}
