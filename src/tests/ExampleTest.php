<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;


class ExampleTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testEnvironmentIsTesting()
    {
        $this->assertEquals(App::environment(), 'testing');
    }

    /**
     * Test that the database pointed to is "comparenigeria_testing'
     * @return void
     */
    public function testDatabaseLoadedIsTestDB()
    {
        $this->assertEquals(config('database.default'), 'mysql');
        $this->assertEquals(config('database.connections.mysql.database'), 'comparenigeria_testing');
    }
}
