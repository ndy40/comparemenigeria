<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Create Retailer
     */
    public function createRetailer($country)
    {
        $retailer = new \Naomi\Entities\Retailer();
        $retailer->id = 1;
        $retailer->enabled = 1;
        $retailer->name="ABC";
        $retailer->country_id = $country->id;
        $retailer->country()->associate($country);

        return $retailer;
    }

    /**
     * Create Country;
     *
     * @return \Naomi\Entities\Country
     */
    public function createCountry()
    {
        $country = new \Naomi\Entities\Country();
        $country->code = "NG";
        $country->enabled = true;
        $country->id = 1;
        $country->name = 'Nigeria';

        return $country;
    }

    public function createProduct($retailer)
    {
        $product = new \Naomi\Entities\Product();
        $product->id = 1;
        $product->name = "Product 1";
        $product->description = "Some description";
        $product->hash = "XXXXX";
        $product->price = 1.0;
        $product->retailer()->associate($retailer);
        $product->retailer_id = $retailer->id;
        $product->sku = '1234';
        $product->url = 'http://somewhere.com';
        $product->created_at = \Carbon\Carbon::now();
        $product->updated_at = \Carbon\Carbon::now();
        $product->deleted_at = null;

        return $product;
    }


}
