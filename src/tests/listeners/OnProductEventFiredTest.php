<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 29/09/16
 * Time: 20:14
 */

use App\Events\ProductEvent;
use App\Listeners\OnProductEventFired;
use \Illuminate\Support\Facades\Redis;

class OnProductEventFiredTest extends TestCase
{

    /**
     * Create ProductEvent
     * @param $type
     * @return ProductEvent
     */
    public function createProductEvent($type)
    {
        $country = $this->createCountry();
        $retailer = $this->createRetailer($country);
        $product = $this->createProduct($retailer);

        $productEvent = new ProductEvent(
            $product,
            $type
        );

        return $productEvent;
    }


    /**
     *  Assert that the array for elastic search is generated.
     * @test
     */
    public function thatGenerateProductIndexFormatReeturnsAnArray()
    {
        $event = new \App\Listeners\OnProductEventFired();
        $productEvent = $this->createProductEvent(ProductEvent::PRODUCT_ADDED_EVENT);
        $dataGenerated = $event->generateProductIndexFormat($productEvent);
        $this->assertCount(2, $dataGenerated);
        $this->assertNotNull($dataGenerated);
        $this->assertEquals($dataGenerated[1]['name'], $productEvent->getProduct()->name);

        $country = $this->createCountry();
        $retailer = $this->createRetailer($country);

        $this->assertEquals(
            $dataGenerated[1]['retailer']['name'],
            $retailer->name
        );
    }

    /**
     * Test that Product is indexed.
     * @test
     */
    public function thatOnProductEventFiredHandleWorks()
    {
        $event = new \App\Listeners\OnProductEventFired();
        $productEvent = $this->createProductEvent(ProductEvent::PRODUCT_ADDED_EVENT);

        $this->assertNotNull($productEvent);
        \Illuminate\Support\Facades\Redis::shouldReceive('lpush')
            ->once()
            ->withAnyArgs()
            ->andReturn(true);

        $event->handle($productEvent);
    }

    /**
     * @expectedException ErrorException
     */
    public function thatOnlyWellFormedProductEventIsFired()
    {
        $productEvent = new ProductEvent(null, ProductEvent::PRODUCT_ADDED_EVENT);
        $event = new OnProductEventFired();

        \Illuminate\Support\Facades\Redis::shouldReceive('lpush')
            ->once()
            ->withAnyArgs()
            ->andReturn(true);

        $event->handle($productEvent);
    }

    /**
     * @test
     */
    public function thatProductIsQueuedForIndexAfterEventIsFired()
    {
        $event = new \App\Listeners\OnProductEventFired();
        $productEvent = $this->createProductEvent(ProductEvent::PRODUCT_ADDED_EVENT);
        $event->handle($productEvent);
        $data = Redis::llen('naomi:products_index');
        $this->assertEquals(1, (int)$data, 'Length matches');

        $dataIndexed = Redis::lrange('naomi:products_index', 0, -1);
        $dataIndex = json_decode($dataIndexed[0], true);

        $this->assertNotNull($dataIndex);
        $this->assertInternalType('array', $dataIndex);

        Redis::del('naomi:products_index');
    }




}