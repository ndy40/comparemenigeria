<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 25/03/16
 * Time: 09:04
 */

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Naomi\Dao\Interfaces\LogicInterface;

class Retailers extends BaseController
{
    /**
     * @var LogicInterface
     */
    protected $retailer;

    public function __construct(LogicInterface $retailer)
    {
        $this->retailer = $retailer;
    }

    public function index()
    {
        $retailers = $this->retailer->all();
        return view("retailers.index", ["retailers" => $retailers]);
    }



}
