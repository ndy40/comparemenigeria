<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 01/01/16
 * Time: 19:07
 */

namespace Naomi\Entities;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Retailer
 * A class representing Retailers.
 *
 * @package Naomi\Entities
 * @property int $id
 * @property string $name - The name of the retailer
 * @property string $homepage - The home page url.
 * @property bool $enabled - Flag to enabled or disable retailer.
 * @property string $logo_path - The path to the retailers logo
 * @property int $country_id - Foreign key to country id on countries table.
 * @property \Naomi\Entities\Country $country
 */
class Retailer extends Model
{
    public function country()
    {
        return $this->belongsTo('Naomi\Entities\Country');
    }
}
