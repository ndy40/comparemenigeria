<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 02/01/16
 * Time: 16:49
 */

namespace Naomi\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * Products being scraped.
 *
 * @package Naomi\Entities
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $ref_url
 * @property string hash
 * @property string $description
 * @property float $price
 * @property float $price_was
 * @property int $retailer_id
 * @property \\Naomi\\Entities\\Retailer $retailer
 * @property string $sku
 * @property date $created_at
 * @property date $updated_at
 * @property bool $deleted_at - Flag to check if product has been deleted.
 */
class Product extends Model
{
    use SoftDeletes;

    protected $dates = array("deleted_at");

    public function scopeAvailable($query)
    {
        return $query->whereNull("deleted_at");
    }

    public function retailer()
    {
        return $this->belongsTo('Naomi\Entities\Retailer');
    }


}
