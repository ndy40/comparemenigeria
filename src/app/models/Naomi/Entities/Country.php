<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 01/01/16
 * Time: 18:59
 */

namespace Naomi\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 * This class represents all the countries in our database.
 *
 * @package Naomi\Entities
 * @property int $id
 * @property string $name
 * @property string $code
 * @property bool $enabled
 * @property \\Illuminate\\Support\\Collection $retailers - List of retailers under this country
 */
class Country extends Model
{
    protected $table = "countries";

    public function retailers()
    {
        return $this->hasMany('Naomi\Entities\Naomi');
    }

}
