<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 22:59
 */

namespace Naomi\Crawler\Abstracts;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Naomi\Crawler\Interfaces\CrawlerInterface;
use Naomi\Crawler\Interfaces\CrawlOptionsInterface;

class RunTaskAbstract implements CrawlerInterface
{
    protected $options = null;

    protected $crawler;

    public function __construct(CrawlOptionsInterface $scrapeOptions)
    {
        $this->options = $scrapeOptions;
        $this->crawler = App::make("CrawlerFactory")->createCrawler($scrapeOptions);
    }

    public function run()
    {
        try {
            $result = $this->crawler->init($this->options)
                ->run()->getResult();
        } catch (\Exception $crawlerException) {
            Log::error("Error ocurred while running crawl - " . $crawlerException->getMessage());
            return false;
        }

        return $result;
    }

    public function getCrawler()
    {
        return $this->crawler;
    }

}
