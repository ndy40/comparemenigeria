<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 28/11/15
 * Time: 10:51
 */

namespace Naomi\Crawler\Exception;


/**
 * Class CrawlerException - Custom exception class.
 *
 * @package Naomi\Crawler\Exception
 */
class CrawlerException extends \Exception
{

}
