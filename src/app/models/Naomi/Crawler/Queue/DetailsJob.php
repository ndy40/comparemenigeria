<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 02/01/16
 * Time: 21:57
 */

namespace Naomi\Crawler\Queue;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Naomi\Crawler\CrawlOptions;
use Naomi\Crawler\Exception\CrawlerException;
use Naomi\Crawler\Execute;
use Naomi\Crawler\Interfaces\QueueJobInterface;
use Naomi\Traits\UtilityTraits;

/**
 * Class DetailsJob
 * Class to queue and process Urls for getting product details.
 *
 * @package Naomi\Crawler\Queue
 */
class DetailsJob extends Job implements
    SelfHandling,
    ShouldQueue,
    QueueJobInterface
{
    use InteractsWithQueue, UtilityTraits;

    protected $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function handle()
    {
        try {
            //At this point we have screen so crawl away.
            $config = new CrawlOptions();
            $config->setProperties($this->config);
            $scrapeLogic = App::make("ScrapeLogic");

            if (!$this->urlExists($config->url)) {
                $this->job->delete();
            } else {
                $crawler = new Execute($config);
                $data    = $crawler->run();

                if (!($data instanceof \DOMDocument)) {
                    throw new CrawlerException("Error occurred during scrape - " . $config);
                }

                $saved = $scrapeLogic->saveProduct($data, $config);

                if (!$saved) {
                    Log::error("Failed to save data for - " . $data->saveXML());
                    if ($this->attempts() < 10) {
                        $this->release(20);
                    } else {
                        $this->job->bury();
                    }
                    return false;
                }
            }
            $this->delete();
            sleep(2);
        } catch (CrawlerException $ex) {
            Log::error($ex->getMessage() . "\n" . $ex->getTraceAsString());
            $this->job->bury();
            return;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage() . "\n" . $ex->getTraceAsString());
            $this->job->bury();
            return;
        }
    }
}
