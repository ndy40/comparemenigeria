<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 04/12/15
 * Time: 20:25
 */

namespace Naomi\Crawler\Queue;


use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Naomi\Crawler\CrawlOptions;
use Naomi\Crawler\Exception\CrawlerException;
use Naomi\Crawler\Execute;
use Naomi\Crawler\Interfaces\QueueJobInterface;


class ListJob extends Job implements
    SelfHandling,
    ShouldQueue,
    QueueJobInterface
{
    use InteractsWithQueue, DispatchesJobs;

    protected $config;

    private $redisProductKey;


    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function handle()
    {
        try {
            //Begin crawl here.
            $config = new CrawlOptions();
            $config->setProperties($this->config);

            $this->redisProductKey = "seed:#prd" . $config->country . "#" . $config->retailer;
            $data = null;

            $crawler = new Execute($config);
            $data = $crawler->run();
            $finished = null;

            if ($data instanceof \DOMDocument) {
                $finished = $this->discoverNewUrls($config, $data);
            } else {
                Log::warning("Empty data returned : " . $config);
            }

            if (!$finished) {
                if ($this->attempts() > 3) {
                    $this->job->bury();
                } else {
                    $this->release("20");
                }
                return;
            }

        } catch (CrawlerException $ex) {
            Log::error("Error occured during crawl - " . $config);
            $this->job->bury();
            return;
        } catch (\Exception $ex) {
            Log::error("System exception - " . $ex->getMessage());
            $this->job->bury();
            return;
        }

        //delete job on complete;
        $this->delete();
        sleep(3);
    }

    /**
     * Queue urls picked up for details scraping.
     *
     * @param CrawlOptions $configContext
     * @param \array $urls
     * @return boolean
     */
    public function queueForDetails($configContext, array $urls)
    {
        try {
            $config = clone $configContext;
            $config->jobType = "details";

            foreach($urls as $url) {
                $config->url = $url;
                $job = (new DetailsJob($config->all()))->onQueue(
                        QueueJobInterface::QUEUE_TUBE_DETAILS_JOB_QUEUE);
                $this->dispatch($job);
            }
        } catch (\Exception $ex) {
            //LOG something here.
            return false;
        }

        return true;
    }

    /**
     * Scan captured URLs against what we already have in our database.
     *
     * @param \Naomi\Crawler\CrawlOptions $config
     * @param \DOMDocument  $data
     * @return boolean
     */
    public function discoverNewUrls(CrawlOptions $config, \DOMDocument $data)
    {
        if (Redis::exists($this->redisProductKey) === false) {
            $productRepo = App::make("ProductLogic");
            $hashes = $productRepo->fetchProductsUrlsWithHashes($config->retailer);

            $productUrls = array();

            if ($hashes->isEmpty()) {
                $domUrls = $data->getElementsByTagName("item");
                foreach($domUrls as $item) {
                    $url = $item->getElementsByTagName("url")->item(0);
                    $productUrls[] = $url->nodeValue;
                }
            } else {
                // Create Redist store record.
                $storeKey = $this->redisProductKey;
                Redis::pipeline(function ($pipe) use ($hashes, $storeKey) {
                    $hashes->each(function ($item) use ($pipe, $storeKey) {
                        $pipe->sadd($storeKey, $item->hash);
                    });
                });

                Redis::expire($storeKey, 10);

                $productUrls = $this->filterNewProductUrls(
                    $this->redisProductKey,
                    $data
                );
            }
        } else {
            $productUrls = $this->filterNewProductUrls(
                $this->redisProductKey,
                $data
            );
        }

        return $this->queueForDetails($config, $productUrls);
    }

    /**
     * This method compares the urls in the XML data against what we have in the database
     * using operations in the Redis store to filter.
     *
     * @param string       $redisKey - The Redis Key to work with.
     * @param \DOMDocument $xmlData
     * @return array - List of product urls that are new.
     */
    protected function filterNewProductUrls($redisKey, \DOMDocument $xmlData)
    {
        $productUrls = array();
        $domUrls = $xmlData->getElementsByTagName("item");

        foreach($domUrls as $item) {
            $url = $item->getElementsByTagName("url")->item(0);
            $hashValue = hash("sha512",$url->nodeValue);
            if (Redis::sismember($redisKey, $hashValue) == 0) {
                $productUrls[] = $url->nodeValue;
            }
        }

        return $productUrls;
    }


}
