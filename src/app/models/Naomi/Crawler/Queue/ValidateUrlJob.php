<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 19/05/16
 * Time: 05:49
 */

namespace Naomi\Crawler\Queue;


use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Redis;
use Naomi\Crawler\DisplayBufferTrait;
use Naomi\Crawler\Interfaces\QueueJobInterface;
use Naomi\Entities\Product;
use Naomi\Traits\UtilityTraits;

class ValidateUrlJob extends Job implements
    QueueJobInterface,
    ShouldQueue,
    SelfHandling
{
    use InteractsWithQueue, UtilityTraits;

    protected $config;

    public function __construct(\stdClass $config)
    {
        $this->config = $config;
    }

    /**
     * @SuppressWarnings(StaticAccess)
     */
    public function handle()
    {
        try {
            if (!$this->urlExists($this->config->url)) {
                Product::destroy($this->config->id);
                //The URL is not okay. So we record.
                $this->countFailedUrl();
            } else {
                //The URL is okay. So we record.
                $this->countOkUrl();
            }
            $this->delete();
            sleep(rand(1, 3));
        } catch (ServerException $ex) {
            Log::warning($ex->getMessage());
            sleep(rand(20, 60));
            $this->release(20);
        }
    }

    protected function countOkUrl()
    {
        if (!Redis::exists("URL_200_OK")) {
            Redis::set("URL_200_OK", 1);
        } else {
            Redis::INCR("URL_200_OK");
        }
    }

    protected function countFailedUrl()
    {
        if (!Redis::exists("URL_400_OK")) {
            Redis::set("URL_400_OK", 1);
        } else {
            Redis::INCR("URL_400_OK");
        }
    }
}
