<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 23:37
 */

namespace Naomi\Crawler\Types;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Naomi\Crawler\DisplayBufferTrait;
use Naomi\Crawler\Exception\CrawlerException;
use Naomi\Crawler\Interfaces\CrawlerUnitOfWork;
use Naomi\Crawler\Interfaces\CrawlOptionsInterface;
use Naomi\Crawler\Interfaces\Illuminate;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;


class Scrape implements CrawlerUnitOfWork
{
    const SERVER_FAILED_TO_START_ERROR = 'xvfb-run failed';

    const CASPERJS_EVALUATION_ERROR = 'casperjs evaluation error';

    /**
     * Result of a scrape.
     *
     * @var string
     */
    public $result;

    /**
     * @var CrawlerOptions
     */
    protected $crawlerOptions;

    public $command;

    public function __construct(CrawlOptionsInterface $options) {
        $this->init($options);
    }

    /**
     * Initialise work.
     *
     * @param \Naomi\Crawler\Interfaces\CrawlOptionsInterface $options
     * @throws \Naomi\Crawler\Exception\CrawlerException
     * @return \Naomi\Crawler\Scrape
     */
    public function init(CrawlOptionsInterface $options)
    {
        $valid = $options->validation();

        if ($valid instanceof MessageBag) {
            $exceptionMessage = "";
            foreach ($valid->all() as $message) {
                $exceptionMessage .= $message . PHP_EOL;
            }
            throw new CrawlerException($exceptionMessage);
        }

        //check if we can find the agent file to begin with.
        $filename = base_path()
            . $options->agentDirectory . "/"
            . $options->country . "/"
            . str_replace('.', '/', $options->agent) . "/"
            . $options->jobType . ".js";

        $this->checkAgentExists($filename);

        //set the crawl option finally.
        $this->crawlerOptions = $options;

        return $this;
    }

    /**
     * Execute scrape task.
     * @throws \Naomi\Crawler\Exception\CrawlerException
     * @return mixed
     */
    public function run()
    {
        $command = $this->buildCommandString();
        $attempts = 0;
//        echo $command; die();

        do {
            $this->result = shell_exec($command);
            $error = $this->checkOutputForError($this->result);

            ++$attempts;
        } while ($error == self::SERVER_FAILED_TO_START_ERROR && $attempts < 4);


        if ($error === self::CRAWL_OUTPUT_ERROR) {
            throw new CrawlerException(
                sprintf(
                    "Error returned from crawler - %s\n%s",
                    $this->result,
                    $command
                )
            );
        }

//        $this->post();

        return $this;

    }

    /**
     * Check if the scrape output threw an error. Return false if no error
     * occurs. Else return on of the error statuses:
     * SERVER_FAILED_TO_START_ERROR | CRAWL_OUTPUT_ERROR | false.
     *
     * @param $output
     * @return bool|string
     */
    public function checkOutputForError($output)
    {
        if (is_null($output)) {
            return self::SERVER_FAILED_TO_START_ERROR;
        } elseif (preg_match('/evaluating/', $output) == 1) {
            return self::CRAWL_OUTPUT_ERROR;
        }

        return false;
    }

    /**
     * Generates the crawler command to use to execute scrape.
     * @return string
     */
    public function buildCommandString()
    {
        $command =  $this->crawlerOptions->command . ' '
            . base_path() . $this->crawlerOptions->script . ' '
            . sprintf(
                '%s.%s.%s',
                $this->crawlerOptions->country,
                $this->crawlerOptions->agent,
                $this->crawlerOptions->jobType
            )
            . ' "' . $this->crawlerOptions->url . '" ';

        if (isset($this->crawlerOptions->logLevel))
        {
            $command .= $this->crawlerOptions->logLevel . " ";
        }

        if ($this->crawlerOptions->verbose === true) {
            $command .= "--verbose ";
        }

        if (isset($this->crawlerOptions->engine)) {
            $command .= "--engine=" . $this->crawlerOptions->engine . " ";
        }

        if ($this->crawlerOptions->showCommand) {
            echo $command . PHP_EOL;
        }

        return $command;
    }

    /**
     * It is called after a crawl run. Can be used to log successful execution
     * or clearing of data from temporary storage, files or memory.
     *
     * @return mixed
     */
    public function post()
    {
        $pid = shell_exec("ps aux | grep -E '[X]vfb :[[:digit:]] | awk '{print $2}'");
        echo $pid;
        if (!is_null($pid)) {
            shell_exec("kill -9 $pid");
        }

        sleep(5);
    }

    /**
     * Check if the Agent file exists.
     *
     * @param $agentFileName
     * @return bool
     */
    protected function checkAgentExists($agentFileName)
    {
        if (!file_exists($agentFileName))
        {
            throw new FileNotFoundException(
                "Agent file {$agentFileName} cannot be found."
            );
        }

        return true;
    }

    /**
     * @return string
     */
    public function getResult()
    {
        $docs = new \DOMDocument('1.0', 'ISO-8859-1');
        $docs->recover = true;
        $docs->loadXML(preg_replace('/&nbsp/i', ' ' ,$this->result));

        return $docs;

    }
}
