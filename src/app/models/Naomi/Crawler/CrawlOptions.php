<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 23:13
 */

namespace Naomi\Crawler;


use Naomi\Crawler\Interfaces\CrawlOptionsInterface;
use Naomi\Traits\BasePropertiesTraits;

/**
 * Class CrawlOptions - A class representing a crawling configuration option.
 *
 * @package Naomi\Crawler
 *
 * @property string $command - Commandline program to invoke. Default is casperjs
 * @property string $script - Name of crawler script "crawler.js" is the default name.
 * @property string $agentFileName - The full path to the agents file including the agents folder.
 * @property string $type - Takes the value scrape or feed.
 * @property string $country - Country code
 * @property string $engine - Takes value phantomjs or slimerjs. SlimerJS pops up a FireFox window during scrape.
 * @property string $jobType - Takes the value list, fetch, details
 * @property Integer $retailer - The database id of this retailer
 * @property Boolean $showCommand - Show the execution command.
 */
class CrawlOptions implements CrawlOptionsInterface
{
    use BasePropertiesTraits;

    public function __construct()
    {
        $this->allowed_values = array(
            "command", "script", "agent", "logLevel", "type", "jobType", "country",
            "proxy", "proxySocket", "verbose", "engine", "url", "agentDirectory",
            'retailer', "showCommand"
        );


        $this->validationRules = array(
            "command"   => "required",
            "script"    => "required|string",
            "agent"     => "required|string",
            'retailer'  => "integer",
            "logLevel"  => "in:debug,info,error,warning",
            "type"      => "required|in:scrape,feed",
            "jobType"   => "required|in:list,fetch,details",
            "country"   => "required|min:2|max:3",
            "engine"    => "in:phantomjs,slimerjs",
            "url"       => "required|url",
            "agentDirectory" => "required",
            "showCommand" => "boolean",
        );
    }

    public function __toString()
    {
        return json_encode($this->all());
    }
}
