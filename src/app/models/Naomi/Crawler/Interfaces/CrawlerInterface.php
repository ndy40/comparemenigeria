<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 22:31
 */

namespace Naomi\Crawler\Interfaces;

use Naomi\Traits\BasePropertiesTraits;

/**
 * Interface CrawlerInterface.
 * Class that all crawling classes must implement.
 *
 * @package Naomi\Crawler\Interfaces
 */
interface CrawlerInterface
{}
