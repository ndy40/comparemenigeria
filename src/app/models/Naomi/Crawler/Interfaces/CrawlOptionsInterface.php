<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 22:37
 */

namespace Naomi\Crawler\Interfaces;


/**
 * Interface CrawlOptionsInterface
 * An interface for crawler options.
 *
 * @package Naomi\Crawler\Interfaces
 */
interface CrawlOptionsInterface
{
}
