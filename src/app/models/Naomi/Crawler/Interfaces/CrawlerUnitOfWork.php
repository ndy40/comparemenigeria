<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 22:51
 */

namespace Naomi\Crawler\Interfaces;

/**
 * Interface CrawlerUnitOfWork defines what constitues a Crawl Task.
 *
 * @package Naomi\Crawler\Interfaces
 */
interface CrawlerUnitOfWork
{
    const CRAWL_OUTPUT_ERROR = "Error occured";

    /**
     * Initialise work.
     *
     * @param \Naomi\Crawler\Interfaces\CrawlOptionsInterface $options
     * @return mixed
     */
    public function init(CrawlOptionsInterface $options);

    /**
     * Execute scrape task.
     *
     * @return string
     */
    public function run();

    /**
     * It is called after a crawl run. Can be used to log successful execution
     * or clearing of data from temporary storage, files or memory.
     * @return mixed
     */
    public function post();
}
