<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 02/01/16
 * Time: 22:24
 */

namespace Naomi\Crawler\Interfaces;


interface QueueJobInterface {

    const QUEUE_TUBE_LIST_JOB_QUEUE = 'lists';

    const QUEUE_TUBE_DETAILS_JOB_QUEUE = 'product_details';

    const QUEUE_TUBE_VALIDATE_URL_JOB_QUEUE = "validate_url";

}
