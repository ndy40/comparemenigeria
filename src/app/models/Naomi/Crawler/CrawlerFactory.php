<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 26/11/15
 * Time: 23:33
 */

namespace Naomi\Crawler;

use Naomi\Crawler\Interfaces\CrawlOptionsInterface;

class CrawlerFactory {

    /**
     * @param \Naomi\Crawler\Interfaces\CrawlOptionsInterface $options
     * @return \Naomi\Crawler\Abstracts\ScrapeAbstract - A subclass of
     * ScrapeAbstract is returned.
     */
    public function createCrawler(CrawlOptionsInterface $options)
    {
        if (isset($options->type)) {
            $class = 'Naomi\\Crawler\\Types\\' . ucfirst($options->type);
            return new  $class($options);
        }

        return null;
    }

}
