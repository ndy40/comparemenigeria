<?php
/**
 * A BasePropertiesTraits PHP client for greeting people
 *
 * Created by PhpStorm.
 * User: ndy40
 * Date: 25/11/15
 * Time: 23:32
 */

namespace Naomi\Traits;


use Illuminate\Support\Facades\Validator;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Traits BaseOptionsTraits
 * A simple utility Trait for classes that need property setting.
 * This will simply setting up getters and setters.
 *
 * @package Naomi\Crawler
 */
trait BasePropertiesTraits {

    /**
     * Array of properties and their values.
     * @var array
     */
    protected $options = array();

    protected $allowed_values = array();

    /**
     * List of properties that are allowed to be set.
     * @var array
     */
    protected $validationRules = array();

    public function __set($name, $value)
    {
        if (in_array($name, $this->allowed_values)) {
            $this->options[$name] = $value;
            return;
        }

        throw new InvalidParameterException("property {$name} not a valid property.");
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->options)) {
            return $this->options[$name];
        }
    }

    /**
     * Get an array list of all properties in the class.
     * @return array
     */
    public function listProperties ()
    {
        return array_keys($this->options);
    }

    public function all()
    {
        return $this->options;
    }

    /**
     * Set all the properties in bulk. Accepts an Associate Array.
     * @param array $properties
     * @return void
     */
    public function setProperties(array $properties)
    {
        foreach ($properties as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Check of a value is set.
     * @param $name Name of property to be checked.
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->options[$name]);
    }

    /**
     * unset/delete value of property to null
     * @param $name
     */
    public function __unset($name)
    {
        unset($this->{$name});
    }

    /**
     * Validate the options set.
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @return mixed
     */
    public function validation()
    {
        $validator = Validator::make($this->options, $this->validationRules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return  $validator->passes();
    }

    /**
     * Get the validation rules set.
     *
     * @return array
     */
    public function getValidationRules()
    {
        return $this->validationRules;
    }
}
