<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 14/04/16
 * Time: 22:50
 */
namespace Naomi\Traits;

use GuzzleHttp\Client;


/**
 * Holds a collection of utility methods for performing simple tasks.
 *
 * @package Naomi\Traits
 */
trait UtilityTraits
{

    /**
     * Utility method to make sure URL exists before scrape starts.
     * Checks for 200 OK message.
     *
     * @param $url
     * @return bool
     */
    public function urlExists($url)
    {
        $http = new Client();
        $response = $http->get($url);
        return ($response->getStatusCode() === 200);
    }

}
