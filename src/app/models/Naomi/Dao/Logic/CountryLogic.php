<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 01/01/16
 * Time: 20:15
 */

namespace Naomi\Dao\Logic;


use Naomi\Dao\Interfaces\LogicInterface;
use Naomi\Dao\Interfaces\RepositoryInterface;

class CountryLogic implements LogicInterface
{
    use ModelTrait;

    public function __construct(RepositoryInterface $repo)
    {
        $this->repository = $repo;
    }

}
