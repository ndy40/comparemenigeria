<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 01/01/16
 * Time: 20:24
 */

namespace Naomi\Dao\Logic;


use Illuminate\Database\Eloquent\Model;

trait ModelTrait
{
    protected $repository;

    /**
     * Fetch all Countries form the table.
     *
     * @param string $orderByColumn
     * @param string $dir
     * @return mixed
     */
    public function all($orderByColumn = "id", $dir = "ASC")
    {
        return $this->repository->all($orderByColumn, $dir);
    }

    /**
     * Find a country by id or throw exception
     * @param $id
     * @param array $columns
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function find($id, $columns = array("*"))
    {
        return $this->repository->find($id, $columns);
    }

    /**
     * Deletes a model.
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->find($id)->delete();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function create(Model $model)
    {
        return $model->save();
    }

    /**
     * Search on a Model by attributes of that model.
     *
     * @param array $columnValues - Format array("clause_type" => array('column', 'op', 'value'))
     * Where 'clause_type' equates to all Laravel supported clauses.
     * @param bool $singleResult - Flag to return the first find if true or return a collection of results.
     * @param array $returnColumns - Select the columns to return.
     */
    public function findByAttributes(
        array $columnValues,
        $returnColumns = array('*')
    ){
        return $this->repository->findByAttributes($columnValues, $returnColumns);
    }

    /**
     * @param array $columnValues
     * @param array $returnColumns
     * @return mixed
     */
    public function findFirstResultByAttributes(
        array $columnValues,
        array $returnColumns = array('*')
    ){
        return $this->repository->findFirstResultByAttributes(
            $columnValues,
            $returnColumns
        );
    }

    /**
     * Convenient method for saving Model
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function update(Model $model)
    {
        return $model->save();
    }

}
