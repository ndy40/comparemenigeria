<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 02/01/16
 * Time: 17:07
 */

namespace Naomi\Dao\Logic;


use Naomi\Dao\Interfaces\LogicInterface;
use Naomi\Dao\Interfaces\RepositoryInterface;

class ProductLogic implements  LogicInterface
{
    use ModelTrait;

    public function __construct(RepositoryInterface $repo)
    {
        $this->repository = $repo;
    }

    /**
     * Return the Hash and url column from products table.
     *
     * @param $retailerId - Database Id of the retailer
     * @return array
     */
    public function fetchProductsUrlsWithHashes($retailerId)
    {
       return $this->repository->fetchProductsUrlsWithHashes($retailerId);
    }

    /**
     * Find Product by HashCode
     * @param $hash
     * @return Collection
     */
    public function findProductByHashCode($hash)
    {
        return $this->repository->findFirstResultByAttributes(array(
            'where' => array('hash', '=', $hash),
        ));
    }
}
