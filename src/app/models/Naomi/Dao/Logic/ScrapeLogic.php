<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 16/01/16
 * Time: 14:48
 */

namespace Naomi\Dao\Logic;

use Naomi\Crawler\Exception\CrawlerException;
use Naomi\Dao\Interfaces\LogicInterface;
use Naomi\Entities\Product;

class ScrapeLogic implements LogicInterface
{
    protected $retailLogic;

    protected $productLogic;

    const PAGE_STATUS_REMOVED = 'removed';

    const PAGE_STATUS_ERROR   = 'error';

    const PAGE_STATUS_ERROR_TAG = 'error';

    const PAGE_STATUS_REMOVED_TAG = 'page';

    public function __construct(ProductLogic $productLogic, RetailLogic $retailLogic)
    {
        $this->productLogic = $productLogic;
        $this->retailLogic  = $retailLogic;
    }

    static function genUrlHash($url)
    {
        return hash("sha512", $url);
    }

    /**
     * Save products from DOMDocument structure.
     * @SuppressWarnings(ElseExpression)
     * @param \DOMDocument $data
     * @param $config
     * @return bool
     * @throws CrawlerException
     */
    public function saveProduct(\DOMDocument $data, $config)
    {
        $status = $this->checkForError($data);

        if ($status === self::PAGE_STATUS_ERROR) {
            throw new CrawlerException("Error with scraped data\n" . $data->saveXML());
        } elseif ($status === self::PAGE_STATUS_REMOVED) {
            $url = $data->getElementsByTagName("url")->item(0)->nodeValue;
            $hash = self::genUrlHash($url);
            $product = $this->productLogic->findProductByHashCode($hash);
            //check if we have the product then delete else proceed.
            if ($product) {
                $this->productLogic->delete($product->id);
            }

            return true;
        }

        $action = "create";
        $retailer = $this->retailLogic->find($config->retailer);
        $product = $this->updateFromXml($data);
        $prevProduct = $this->productLogic->findProductByHashCode($product->hash);

        //TODO: Check if product needs to be deleted.
        if ($prevProduct) {
            $product = $this->updateProductChanges($prevProduct->first(), $product);
            $action = 'update';
        } else {
            $product->retailer()->associate($retailer);
        }

        return $this->productLogic->{$action}($product);
    }

    /**
     * Generate Product object from XML docs.
     *
     * @param \DOMDocument $data
     * @return \Naomi\Entities\Product
     */
    public function updateFromXml(\DOMDocument $data)
    {
        $product = new Product;
        $properties = array("name", "price", "price_was", "description", "sku", "url");
        array_walk($properties, function ($prop) use ($data, &$product) {
            $item = $data->getElementsByTagName($prop);
            if ($item->length > 0) {
                $product->$prop = $item->item(0)->nodeValue;
            }
        });

        $product->hash = self::genUrlHash($product->url . $product->sku);

        return $product;
    }

    public function updateProductChanges(Product $prevProduct, Product $dataProduct)
    {
        $attributes = $dataProduct->getAttributes();
        foreach ($attributes as $attribute) {
            $prevProduct->$attribute = addslashes($dataProduct->$attribute);
        }

        return $prevProduct;
    }

    public function deleteProduct($url)
    {
        $product = $this->productLogic->findProductByHashCode(
            self::genUrlHash($url),
            true
        );

        if ($product) {
            return $this->productLogic->delete($product->id);
        }

        return false;
    }

    /**
     * Check if the xml data from scrape has error tag.
     *
     * @param \DOMDocument $data
     * @return bool|string
     */
    public function checkForError(\DOMDocument $data)
    {
        $errorTage = $data->getElementsByTagName(self::PAGE_STATUS_ERROR_TAG);

        if ($errorTage->length > 0) {
            return self::PAGE_STATUS_ERROR;
        }

        $pageRemoved = $data->getElementsByTagName(self::PAGE_STATUS_REMOVED_TAG);
        if ($pageRemoved->length > 0 ) {
            return self::PAGE_STATUS_REMOVED;
        }

        return false;
    }



}
