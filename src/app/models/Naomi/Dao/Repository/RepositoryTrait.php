<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 02/01/16
 * Time: 16:40
 */

namespace Naomi\Dao\Repository;


trait RepositoryTrait {

    public $model;

    /**
     * Fetch all Countries form the table.
     *
     * @param string $orderByColumn
     * @param string $dir
     * @return mixed
     */
    public function all($orderByColumn = "id", $dir = "ASC")
    {
        return $this->model()->orderBy($orderByColumn, $dir)->get();
    }

    /**
     * Find a country by id or throw exception
     * @param $id
     * @param array $columns
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function find($id, $columns = array("*"))
    {
        return $this->model()->findOrFail($id, $columns);
    }

    /**
     * Return Model instance.
     * @return mixed
     */
    public function model()
    {
        return new $this->model();
    }

    /**
     * Search on a Model by attributes of that model.
     *
     * @param array $columnValues - Format array("clause_type" => array('column', 'op', 'value'))
     * Where 'clause_type' equates to all Laravel supported clauses.
     * @param array $returnColumns - Select the columns to return.
     */
    public function findByAttributes(array $columnValues, $returnColumns = array('*'))
    {
        $model = $this->model();
        foreach ($columnValues as $key => $value) {
            $model = $model->{$key}($value[0], $value[1], $value[2]);
        }

        return $model->get($returnColumns);
    }

    /**
     * Return the first result found that matches attributes searched for.
     *
     * @param array $columnValues
     * @param array $returnColumns
     * @return Model
     */
    public function findFirstResultByAttributes(
        array $columnValues,
        $returnColumns = array('*')
    ){
        $model = $this->model();
        foreach ($columnValues as $key => $value) {
            $model = $model->{$key}($value[0], $value[1], $value[2]);
        }

        return $model->first($returnColumns);
    }

}
