<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 01/01/16
 * Time: 19:53
 */

namespace Naomi\Dao\Repository;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Naomi\Dao\Interfaces\RepositoryInterface;
use Naomi\Entities\Country;

/**
 * Class CountryRepository
 * CountryRepository class for accessing the Countries table.
 *
 * @package Naomi\Dao\Repository
 */
class CountryRepository implements  RepositoryInterface
{
    /**
     * Fetch all Countries form the table.
     *
     * @param string $orderByColumn
     * @param string $dir
     * @return mixed
     */
    public function all($orderByColumn = "id", $dir = "ASC")
    {
        return Country::orderBy($orderByColumn, $dir)->get();
    }

    /**
     * Find a country by id or throw exception
     *
     * @SuppressWarnings(StaticAccess)
     * @param $id
     * @param array $columns
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function find($id, $columns = array("*"))
    {
        return Country::findOrFail($id, $columns);
    }
}
