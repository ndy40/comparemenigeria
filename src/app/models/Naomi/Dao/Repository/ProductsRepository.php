<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 02/01/16
 * Time: 16:36
 */

namespace Naomi\Dao\Repository;


use Naomi\Dao\Interfaces\RepositoryInterface;
use Naomi\Entities\Product;

class ProductsRepository implements RepositoryInterface
{
    use RepositoryTrait;

    public function __construct()
    {
        $this->model = Product::class;
    }

    /**
     * Return the Hash and url column from products table.
     *
     * @param $retailerId - Database Id of the retailer
     * @return array
     */
    public function fetchProductsUrlsWithHashes($retailerId)
    {
        $products =  $this->model() //->available()
            ->where("retailer_id", "=", $retailerId)
            ->get(array("hash", "url"));

        return $products;
    }
}
