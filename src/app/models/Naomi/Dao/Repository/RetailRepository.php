<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 01/01/16
 * Time: 21:04
 */

namespace Naomi\Dao\Repository;


use Naomi\Dao\Interfaces\RepositoryInterface;
use Naomi\Entities\Retailer;

class RetailRepository implements RepositoryInterface
{
    use RepositoryTrait;

    public function __construct()
    {
        $this->model = 'Naomi\Entities\Retailer';
    }


}
