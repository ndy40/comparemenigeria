<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Commands\Naomi\CrawlCommand::class,
        \App\Commands\Naomi\ScheduleCrawl::class,
        \App\Commands\Naomi\ValidateURLCommand::class,
        \App\Commands\Naomi\UpdateIndex::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        //validate product command.
        $schedule->command("naomi:check-products")
            ->weekly()->withoutOverlapping();

//        $schedule->command('naomi:updateindex')->everyThirtyMinutes()
//            ->withoutOverlapping();
    }
}
