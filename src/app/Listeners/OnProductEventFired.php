<?php

namespace App\Listeners;

use App\Events\ProductEvent;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;

class OnProductEventFired
{

    /**
     * Handle the event.
     *
     * @param  ProductEvent  $event
     * @return void
     */
    public function handle(ProductEvent $event)
    {
        $data = $this->generateProductIndexFormat($event);

        Redis::lpush('naomi:products_index', json_encode($data));
    }

    /**
     * Format data into array structure required by ElasticSearch Client Library.
     * @see https://www.elastic.co/guide/en/elasticsearch/client/php-api/current/_indexing_documents.html
     * @param ProductEvent $event
     * @return array
     */
    public function generateProductIndexFormat(ProductEvent $event)
    {
        $data = [];
        $product = $event->getProduct();
        $data[] = [
            $event->getEventType() => [
                '_index' => config('elasticsearch.index.default'),
                '_type'  => 'product',
                '_id'    => $product->id,
            ]
        ];


        $data[] = [
            'name'          => $product->name,
            'url'           => $product->url,
            'price'         => $product->price,
            'price_was'     => $product->price_was,
            'sku'           => $product->sku,
            'description'   => $product->description,
            'last_updated'  => $product->updated_at->toDateTimeString(),
            'first_seen'    => $product->created_at->toDateTimeString(),
            'retailer'      => [
                'name'  => $product->retailer->name,
                'id'    => $product->retailer->id,
                'logo'  => $product->retailer->logo_path,
                'url'   => $product->retailer->homepage,
            ]
        ];

        return $data;
    }
}
