<?php

namespace App\Providers;

use App\Events\ProductEvent;
use App\Http\Controllers\Retailers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Naomi\Crawler\CrawlerFactory;
use Naomi\Dao\Logic\ProductLogic;
use Naomi\Dao\Logic\ScrapeLogic;
use Naomi\Dao\Repository\ProductsRepository;
use Naomi\Entities\Product;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when('Naomi\Dao\Logic\RetailLogic')
            ->needs('Naomi\Dao\Interfaces\RepositoryInterface')
            ->give('Naomi\Dao\Repository\RetailRepository');

        $this->app->bind('ProductLogic', function($app){
            return new ProductLogic(new ProductsRepository());
        });

        $this->app->when('App\Commands\Naomi\ScheduleCrawl')
            ->needs('Naomi\Dao\Interfaces\LogicInterface')
            ->give('Naomi\Dao\Logic\RetailLogic');

        $this->app->when('Naomi\Crawler\Queue\DetailsJob')
            ->needs('Naomi\Dao\Interfaces\LogicInterface')
            ->give(function () {
                return App::make("ProductLogic");
            });

        $this->app->when('App\Commands\Naomi\CrawlCommand')
            ->needs('Naomi\Dao\Interfaces\LogicInterface')
            ->give(function () {
                return App::make("ProductLogic");
            });

        $this->app->bind("ScrapeLogic", function ($app) {
            return new ScrapeLogic(
                $app["ProductLogic"],
                $app['Naomi\Dao\Logic\RetailLogic']
            );
        });

        $this->app->when('App\Http\Controllers\Retailers')
            ->needs('Naomi\Dao\Interfaces\LogicInterface')
            ->give('Naomi\Dao\Logic\RetailLogic');

        $this->app->singleton("CrawlerFactory", function () {
            return new CrawlerFactory();
        });

        // Product Create, Delete, Update event.
        Product::created(function ($product) {
            Event::fire(new ProductEvent($product, ProductEvent::PRODUCT_ADDED_EVENT));
        });

        Product::updated(function ($product) {
            Event::fire(new ProductEvent($product, ProductEvent::PRODUCT_UPDATED_EVENT));
        });

        Product::deleted(function ($product) {
            Event::fire(new ProductEvent($product, ProductEvent::PRODUCT_DELETED_EVENT));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
