<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Naomi\Entities\Product;

/**
 * Class ProductEvent
 * @package App\Events
 */
class ProductEvent extends Event
{
    use SerializesModels;

    /**
     *  Represents a product added event.
     */
    const PRODUCT_ADDED_EVENT   = 'index';

    /**
     *  Represents a product deleted event.
     */
    const PRODUCT_DELETED_EVENT = 'delete';

    /**
     *  Represents a product updated event.
     */
    const PRODUCT_UPDATED_EVENT = 'update';

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var string
     */
    protected $eventType;

    /**
     * Create a new event instance.
     * @param Product $product An instance of Product.
     * @param string $eventType The type of product event happening (added | deleted | updated)
     */
    public function __construct(Product $product, $eventType = ProductEvent::PRODUCT_ADDED_EVENT)
    {
        $this->product = $product;

        $this->eventType = $eventType;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    /**
     * Get the Type of event. Possible values are: added, deleted, updated.
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Get the Product attached to this event.
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
