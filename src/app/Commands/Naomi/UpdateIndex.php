<?php

namespace App\Commands\Naomi;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\App;

class UpdateIndex extends Command implements SelfHandling
{
    protected $description =
        'Picks up cached products for indexing from redis and indexes them.';

    protected $signature = 'naomi:updateindex';

    /*
     * Instance of ElasticSearch client.
     * @var
     */
    protected $esClient;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->esClient = App::make('elasticsearch');
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $currentLength = Redis::llen('naomi:products_index');

        $bar = $this->output->createProgressBar($currentLength - 1);

        $productLists = Redis::lrange('naomi:products_index', 0, $currentLength);

        $dataToIndex = ['body' => []];

        foreach ($productLists as $product) {
            $dataToIndex['body'] = array_merge($dataToIndex['body'], json_decode($product, true));
            $bar->advance();
        }

        //Index data collected.
        // TODO: Log response from Index.
        $response = $this->indexProducts($dataToIndex);

        $this->info('Cleaning up');

        // Clean up redis store.
        $this->cleanup($currentLength);
        $bar->finish();
    }

    protected function indexProducts(array $data)
    {
        return $this->esClient->bulk($data);
    }

    /**
     * Clear the Redis Cache after indexing.
     *
     * @param int $length
     */
    protected function cleanup($length)
    {
        for($i = 0; $i <= $length; $i++) {
            Redis::lpop('naomi:products_index');
        }
    }
}
