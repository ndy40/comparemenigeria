<?php

namespace App\Commands\Naomi;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\Log;
use Naomi\Dao\Interfaces\LogicInterface;

class ScheduleCrawl extends Command implements SelfHandling
{
    protected $description  = "Commend to schedule different types of crawl tasks.";

    protected $signature    = "naomi:run {retailer : Database Id of the retailer} {job_type=list : JobType takes values list or details}";

    /**
     * Holds and instance of the RetailerLogic.
     *
     * @var \Naomi\Dao\Interfaces\LogicInterface
     */
    protected $retailerLogic;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(LogicInterface $retailer)
    {
        parent::__construct();
        $this->retailerLogic = $retailer;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $retailerId = $this->argument("retailer");
        $retailer = $this->retailerLogic->find($retailerId);

        if (!$retailer->enabled) {
            $this->info("Retailer {$retailer} not enabled");
            Log::info("Retailer {$retailer} not enabled. Scrape can't run.");
            return;
        }

        $data = array(
            "country"       => $retailer->country->code,
            "job_type"      => $this->argument("job_type"),
            "agent_dir"     => $retailer->directory_name,
            "url"           => $retailer->homepage,
            "--type"        => $retailer->type,
            "--verbose"     => $this->option("verbose"),
            "--retailer"    => $retailerId,
        );

        if ($this->option("verbose")) {
            $this->call("naomi:exec", $data);
        } else {
            $this->callSilent("naomi:exec", $data);
        }
    }
}
