<?php

namespace App\Commands\Naomi;

use GuzzleHttp\Exception\ServerException;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Naomi\Crawler\Interfaces\QueueJobInterface;
use Naomi\Crawler\Queue\ValidateUrlJob;
use Naomi\Entities\Product;
use Naomi\Traits\UtilityTraits;

class ValidateURLCommand extends Command implements SelfHandling
{
    use DispatchesJobs, UtilityTraits;

    protected $description = "Queue URLs for validation. All 404 URLs will be removed.";

    protected $signature = "naomi:check-products";


    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $self = $this;
        Redis::del("URL_200_OK", "URL_400_OK");

        DB::table("products")->select("id", "url")->orderBy("id")->whereNull("deleted_at")
            ->chunk(100, function ($rows) use ($self) {
                foreach($rows as $row) {
                    $product = new \stdClass();
                    $product->id = $row->id;
                    $product->url = $row->url;
                    //Queue Job for processing.
                    $job = (new ValidateUrlJob($product))->onQueue(QueueJobInterface::QUEUE_TUBE_VALIDATE_URL_JOB_QUEUE);
                    $self->dispatch($job);
                }
            });
    }

}
