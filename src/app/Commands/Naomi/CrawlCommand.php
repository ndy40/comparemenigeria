<?php

namespace App\Commands\Naomi;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Config;
use Naomi\Crawler\CrawlOptions;
use Naomi\Crawler\Execute;
use Naomi\Crawler\Interfaces\QueueJobInterface;
use Naomi\Crawler\Queue\DetailsJob;
use Naomi\Crawler\Queue\ListJob;
use Naomi\Dao\Interfaces\LogicInterface;

class CrawlCommand extends Command implements SelfHandling
{
    use DispatchesJobs;

    /**
     * @var \Naomi\Dao\Interfaces\LogicInterface
     */
    protected $productRepo;

    protected $description = "Command for initiating a crawl.";


    protected $signature = "naomi:exec {country} {agent_dir} {url}
        {job_type=list : Takes value list/fetch/details} {--type=scrape : The type of crawl scrape/feed} {--engine=slimerjs : Takes phantomjs/slimerjs}
        {--logLevel= : Values accepted info, warning, debug, error} {--retailer}";

    protected $crawlConfig;

    /**
     * Create a new command instance.
     *
     */
    public function __construct(LogicInterface $productLogic)
    {
        parent::__construct();
        $this->productRepo = $productLogic;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {

        $this->buildConfig();
        if ($this->crawlConfig->jobType == 'list') {
            $this->{$this->crawlConfig->type}();
        } elseif ($this->crawlConfig->jobType == 'details') {
            $this->productUpdate();
        }
    }

    /**
     * Perform JavaScript based Scraping.
     */
    public function scrape()
    {

        $crawler = new Execute($this->crawlConfig);
        $data = $crawler->run();

        $queue = $this->crawlConfig->jobType . "Queue";

        $this->{$queue}($data, $this->crawlConfig->jobType);
    }

    /**
     * Perform PHP Feed based scraping.
     */
    public function feed()
    {
        $this->info("Feed got called.");
    }

    /**
     * Set all the configuration data
     *
     * @return void
     */
    protected function buildConfig()
    {
        $scrapeType = $this->option("type");

        $this->crawlConfig = new CrawlOptions();
        $this->crawlConfig->command         = Config::get("scrape.{$scrapeType}.command");
        $this->crawlConfig->script          = Config::get("scrape.{$scrapeType}.script");
        $this->crawlConfig->agentDirectory  = Config::get("scrape.{$scrapeType}.agent_dir");

        $this->crawlConfig->type        = $this->option("type");
        $this->crawlConfig->retailer    = $this->option("retailer");
        $this->crawlConfig->jobType     = $this->argument("job_type");
        $this->crawlConfig->engine      = $this->option("engine");
        $this->crawlConfig->verbose     = $this->option("verbose");
        $this->crawlConfig->logLevel    = $this->option("logLevel");

        //Scrape specific values;
        $this->crawlConfig->country = $this->argument("country");
        $this->crawlConfig->agent   = $this->argument("agent_dir");
        $this->crawlConfig->url     = $this->argument("url");
    }

    /**
     * Put all the scraped catalogue URls into queue for processing.
     * @param \DOMDocument $data
     */
    protected function listQueue(\DOMDocument $data)
    {
        $items = $data->getElementsByTagName("item");
        $config = clone $this->crawlConfig;
        $config->jobType = "fetch";

        foreach ($items as $item) {
            $url = $item->firstChild->nodeValue;
            $config->url = $url;
            $job = (new ListJob($config->all()))->onQueue(QueueJobInterface::QUEUE_TUBE_LIST_JOB_QUEUE);
            $this->dispatch($job);
        }
    }


    /**
     * Simply query for all products belonging to the Retailer and perform a Details scrape for updates.
     */
    protected function productUpdate()
    {
        $config = clone $this->crawlConfig;

        $collection = $this->productRepo->findByAttributes(
            array("where" => array("retailer_id", "=", $this->crawlConfig->retailer)),
            false,
            array("url")
        );

        $config->jobType = 'details';

        foreach ($collection->all() as $product) {
            $config->url = $product->url;
            $job = (new DetailsJob($config->all()))->onQueue(QueueJobInterface::QUEUE_TUBE_DETAILS_JOB_QUEUE);
            $this->dispatch($job);
        }
    }
}
