var webpack = require('webpack');

module.exports = {
    entry: './resources/assets/js/main.ts',
    output: {
        path: './public/dist',
        filename: 'app.bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader', 'angular2-template-loader']
            },
            {test: /\.html/, loader: 'html'}
        ]
    },
    resolve: {
        extensions: ['', '.js', '.ts']
    }
}