<?php
/**
 * Created by PhpStorm.
 * User: ndy40
 * Date: 28/11/15
 * Time: 14:03
 */

return array(
    'scrape' => [
        'command'   => '/usr/bin/casperjs',
        'script'    => '/crawler/crawler.js',
        'agent_dir' => '/crawler/agents',
    ],
    'display' => [
        'min_screen' => 0,
        'max_screen' => 10,
        'screen_id'  => 1
    ]

);
