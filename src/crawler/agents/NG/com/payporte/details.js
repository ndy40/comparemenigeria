var require = patchRequire(require),
    sys = require("system"),
    utils = require("utils");


exports.create = function () {
    'use strict';
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages : false,
            userAgent : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7"
        },

        //BLock contents.
        onResourceRequested: function (casper, requestData, networkData) {
            if (/analytics|facebook|conversion|google|images|twitter|addthis|cloudfront|track|tag|criteo|css|media\images/ig.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casper, onComplete) {
    'use strict';
    casper.waitForSelector("button#addtocart", function () {
        var dataType = require(sys.env['CRAWLERPATH'] + "/lib/datatype");
        var results = this.evaluate(function () {
            var name      = __utils__.findOne("#product_addtocart_form h3"),
                price     = __utils__.findOne("span.regular-price[id] span.price"),
                price_was = __utils__.findOne(".purchase-actions .previous-price"),
                inStock   = __utils__.exists("button#addtocart"),
                sku       = __utils__.getElementByXPath("//*[contains(@class, 'product-info-wrap')]//*[contains(@class, 'panel-body') and contains(., 'SKU')]")
                    .textContent.replace(/Product.+:|\s/g,'');

            return {
                name        : (name !== null ? name.textContent.trim() : null),
                price       : (price !== null ? price.textContent.trim() : null),
                price_was   : ( price_was !== null ? price_was.textContent.trim() : null),
                inStock     : inStock,
                description : __utils__.findAll(".product-info-wrap div.panel-default:nth-of-type(1)")
                    .map(function (e) { return e.textContent.trim(); }).join(" "),
                sku    : (sku !== null ? sku : null)
            };
        });

        onComplete({
            name      : dataType.strings(results.name),
            price     : dataType.numbers(results.price, ','),
            price_was : dataType.numbers(results.price_was, ','),
            inStock   : dataType.boolean(results.inStock),
            description : dataType.strings(results.description),
            sku       : dataType.strings(results.sku)
        });

    }, function () {
        this.waitForSelector("*.no-result-suggestions", function () {
            onComplete({ page : "removed" });
        }, function () {
            onComplete({ error : "Page Error" });
        });
    });
};
