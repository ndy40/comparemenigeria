var require = patchRequire(require);

exports.create = function () {
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages : false,
            userAgent : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7"
        },

        //BLock contents.
        onResourceRequested: function (casper, requestData, networkData) {
            if (/analytics|facebook|conversion|google|images|twitter|addthis|cloudfront|track|tag|criteo/ig.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casperjs, callback) {
    casperjs.waitForSelector("#product-list .product-name", function () {
        this.repeat(10, function () {
            this.click('#load-more-button.loading-btn');
            this.waitWhileSelector('#load-more-button.loading-btn.loading-animate', function () {
               this.wait(7000);
            });
        });
    }).then(function () {
        callback(this.evaluate(function () {
            var links =  __utils__.findAll("#product-list .product-name a");
            return links.map(function (e) {
                return {
                    url : e.href
                };
            });
        }));
    });
};