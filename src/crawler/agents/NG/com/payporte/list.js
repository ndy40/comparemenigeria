var require = patchRequire(require);

exports.create = function () {
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages: false
        },
        //BLock contents.
        onResourceRequested : function (casper, requestData, networkData) {
            'use strict';
            if (/track|talk|google|twitter|addthis|facebook|postmessage|css/i.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casperjs, callback) {
    'use strict';
    var data =  casperjs.evaluate(function () {
        var urls =  __utils__.findAll(".category-listing a");
        return urls.filter(function (e) {
            return !/category|&|search|\?/.test(e.href)
        }).map(function (e) {
            return  {"url" : e.href };
        });
    });
    callback(data);
};