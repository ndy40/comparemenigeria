var require = patchRequire(require);

exports.create = function () {
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages : false,
            userAgent : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7"
        },

        //BLock contents.
        onResourceRequested: function (casper, requestData, networkData) {
            if (/analytics|facebook|conversion|google|images|twitter|addthis|cloudfront|track|tag|criteo|css/ig.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casperjs, callback) {
    var output,scrollBack;

    scrollBack = function(pages) {
        if (pages > 0) {
            casperjs.scrollToBottom();
            casperjs.wait(2000, function () { scrollBack(pages - 1 );});
        }
    };

    casperjs.waitForSelector("ul.catalog", function () {
        scrollBack(3);
    }).then(function () {
         callback(this.evaluate(function () { 
            var links =  __utils__.findAll("*.product-block a.product-block-link");
                return links.map(function (e) {
                    return {
                        url : e.href
                    };
                }); 
        }));
        
    });
};