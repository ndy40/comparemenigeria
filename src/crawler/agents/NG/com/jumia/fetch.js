var require = patchRequire(require);

exports.create = function () {
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages : false,
            userAgent : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7"
        },

        //BLock contents.
        onResourceRequested: function (casper, requestData, networkData) {
            if (/static\.jumia\.com/i.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casperjs, callback) {
    'use strict';
    var cacheUrls,
        outputBuffer = [], 
        pageUrls;

    pageUrls = casperjs.evaluate(function() {
        var urls =  __utils__.findAll("*.osh-row.bottom li.item a");
        return urls.filter(function (e) { 
            return /\d+/g.test(e.title) && e.title < 4; 
        })
        .map(function (e) { 
            return e.href;
        });
    });

    cacheUrls = function () {
        var urls = casperjs.evaluate(function () {
            var url =  __utils__.findAll("div[data-sku] > a.link[href*=html]");
            return url.map(function (e) {
                return {url : e.href};
            });
        });
        return urls; 
    };
    
    casperjs.each(pageUrls, function(self, element) {
        outputBuffer = outputBuffer.concat(cacheUrls());
    });

    callback(outputBuffer)    
};