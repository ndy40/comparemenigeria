var require = patchRequire(require),
    sys = require("system"),
    utils = require("utils");
    

exports.create = function () {
    'use strict';
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages : false,
            userAgent : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7"
        },

        //BLock contents.
        onResourceRequested: function (casper, requestData, networkData) {
            if (/analytics|facebook|conversion|google|images|twitter|addthis|cloudfront|track|tag|criteo|css/ig.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casper, onComplete) {
    'use strict';
    var dataOptions = {
        "name" : "string",
        "price" : "numeric",
        "price_was" : "numeric",
        "inStock" : "boolean",
        "description" : "string",
        "sku" : "string"
    };

    casper.waitUntilVisible(".details-footer button.-add-to-cart", function () {
        var dataType = require(sys.env['CRAWLERPATH'] + "/lib/datatype");
        var results = this.evaluate(function () {
            var name  = __utils__.findOne("h1.title"),
                price     = __utils__.findOne(".details-footer > .price-box span[data-price]"),
                price_was = __utils__.findOne(".purchase-actions .previous-price"),
                inStock   = __utils__.exists(".actions button.osh-btn.-primary"),
                sku       = __utils__.findOne("*.actions button.osh-btn.-primary");

            return {
                name        : (name !== null ? name.textContent.trim() : null),
                price       : (price !== null ? price.textContent.trim() : null),
                price_was   : ( price_was !== null ? price_was.textContent.trim() : null),
                inStock     : inStock,
                description : __utils__.findAll("#productDescriptionTab")
                    .map(function (e) { return e.textContent.trim(); }).join(" "),
                sku    : (sku !== undefined ? sku.dataset.skuSimple : null)
            };
        });
        
        onComplete({
            name      : dataType.strings(results.name),
            price     : dataType.numbers(results.price, ','),
            price_was : dataType.numbers(results.price_was, ','),
            inStock   : dataType.boolean(results.inStock),
            description : dataType.strings(results.description),
            sku       : dataType.strings(results.sku),
        });

    }, function () {
        this.waitForSelector("*.no-result-suggestions", function () {
            onComplete({ page : "removed" });
        }, function () {
            onComplete({ error : "Page Error" });
        });
    });
};
