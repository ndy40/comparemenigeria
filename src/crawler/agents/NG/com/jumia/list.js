var require = patchRequire(require);

exports.create = function () {
    return {
        pageSettings: {
            loadPlugins: false,
            loadImages: false
        },
        //BLock contents.
        onResourceRequested : function (casper, requestData, networkData) {
            'use strict';
            if (/static\.jumia\.com/i.test(requestData.url)) {
                networkData.abort();
            }
        }
    };
};

exports.run = function (casperjs, callback) {
    'use strict';
    var data =  casperjs.evaluate(function () {
        var urls =  __utils__.findAll(".submenu a.subcategory");
        return urls.filter(function (e) {
                return /www\.jumia\.com/i.test(e.href)
            })
            .map(function (e) {
                return  {"url" : e.href };
            });
    });
    callback(data);
};