var require = patchRequire(require),
    output,
    args, 
    opts,
    isValid,
    casperjs = require("casper").create({viewportSize: {width: 1200, height: 800}}),
    sys      = require("system"),
    utils    = require("utils"),
    utility  = require(sys.env['CRAWLERPATH'] + "/lib/utility");

/**
 * Handle framework errors here.
 */
casperjs.options.onError = function (msg, stack) {
    casperjs.echo(msg);
    stack.forEach(function(element) {
        casperjs.echo(element);
    }, this);
};

casperjs.on("scrape.fail", function (data) {
    casperjs.echo("Scraped Failed.");
    utils.dump(data);
});

args = casperjs.cli.args;
opts = casperjs.cli.options;
isValid = utility.validateScriptArgs(args);

if (isValid !== true) {
    if (casperjs.cli.has("verbose")) {
        casperjs.echo("Arguments");
        utils.dump(casperjs.cli.args);
        casperjs.echo("Options");
        utils.dump(casperjs.cli.options);
    }
    casperjs.echo("Scrape Error:\n" + isValid, "ERROR");
    casperjs.exit(1);
}

var agent = utility.buildAgentInstance(casperjs);

casperjs.start(args[1], function () {
    var options = agent.create(),
        keys = Object.keys(options),
        self = this;
    if (Array.isArray(keys)) {
        keys.forEach(function(element) {
            self.options[element] = options[element];
        }, this);
    }
}).then(function () {
    agent.run(this, function (data) {
        var requestData = utility.buildRequestOutput(utility.headers(casperjs.currentResponse));
        var dataOutput  =  utility.buildResultOutput(data);
        output = "<xml>" + requestData + dataOutput + "</xml>";
    });
});

casperjs.run(function () {
    this.echo(output);
    this.exit(0);
});