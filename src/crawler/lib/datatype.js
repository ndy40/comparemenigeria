/**
 * @name Naomi DataType Converters
 *  File containing useful datatype formatters.
 * @author Ndifreke Ekott <ndy40.ekott@gmail.com>
 */

/**
 * A short list of ampersand strings to substitute with.
 */

var ampsToReplace = {
    '&nbsp;': ' '
};

function replaceTag(tag) {
    return ampsToReplace[tag] || tag;
};

/**
 *  Replaces all tags in a string.
 */
function safe_tags_replace(str) {
    return str.replace(/(&\w+;)/g, replaceTag).replace(/&lt;\w+.*&gt;|\n/g,'');
};

function fnString(params) {
    'use strict';
    if (params !== undefined) {
        return safe_tags_replace(params);   
    }
    
    return params;
};

function fnNumeric(params, replacement) {
    'use strict';
    if (params !== undefined && params !== null) {
        return params.replace(/[^\d+\.,]/g, '').replace(/,/g, '');
    }

    return null;
};

function fnBoolean(params) {
    'use strict';
    return (params !== undefined ? true : false);
};

var processor = {
  "string"  : fnString,
  "numeric" : fnNumeric,
  "boolean" : fnBoolean
};

/**
 * String method for stripping values of special HTML Tags. 
 * @module datatype/string
 */
exports.strings = function (value) {
    'use strict';
    return processor["string"](value);
};

/**
 *  Currency stripping funciton.
 */
exports.numbers = function (value) {
    'use strict';
    return processor['numeric'](value);
};

exports.boolean = function (value) {
    'use strict';
    return processor['boolean'](value);
};

