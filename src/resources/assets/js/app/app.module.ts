import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DashboardRoutingModule } from './dashboard/dashboard.routing.module';
import { RetailerRoutingModule } from './retailers/retailer-routing.module';

@NgModule({
    imports: [
        BrowserModule, 
        SharedModule, 
        DashboardRoutingModule,
        RetailerRoutingModule
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule {}