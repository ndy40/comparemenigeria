import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
    <div class="container-fluid">
        <nav class="nav" style="background-color: #e5f0f9;">
            <div class="nav-left">
                <a class="nav-item is-brand" routerLink='/'>
                    <span class="icon">
                            <i class="fa fa-home" aria-hidden></i>
                    </span>
                </a>
                <a class="nav-item" routerLink='/products'>
                    <span class="icon">
                        <i class="fa fa-product-hunt"></i>
                    </span>
                    Product
                </a>
                <a class='nav-item' routerLink='/retailers'>
                    Retailer
                </a>
                <a class='nav-item' routerLink='/queue'>
                    Queue
                </a>
                <a class="nav-item" routerLink='/searchengine'>
                    Search Engine
                </a>
            </div>
        </nav>
        <router-outlet></router-outlet>
    </div>
    `
})
export class AppComponent {}