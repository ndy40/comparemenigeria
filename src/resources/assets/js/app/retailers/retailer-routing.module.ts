import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { RetailerListsComponent } from './retailer-lists.component';
import { RetailerAddComponent } from './retailer-add.component';


let routes : Routes = [
    {
        path: "retailers",
        component: RetailerListsComponent,
        pathMatch: "full"
    },
    {
        path: "retailers/add",
        component: RetailerAddComponent,
        pathMatch: "full"
    }
];

@NgModule({
    imports: [
        SharedModule, 
        RouterModule.forRoot(routes)
    ],
    declarations: [RetailerAddComponent, RetailerListsComponent], 
    exports: [RetailerAddComponent, RetailerListsComponent]
})
export class RetailerRoutingModule {}

