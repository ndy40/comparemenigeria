
class BaseModel {
    id: number;
}


/**
 *  A simple Country class.
 */
class Country extends BaseModel {
    name: string;
    code: string;
    enabled: boolean;
}


/**
 *  Currency Class.
 */
class Currency extends BaseModel {
    currency: string;
    minor_units: number;
    prefix: string;
    suffix: string;
    decimal_separator: string = '.';
}

class Retailer extends BaseModel {
    country_id: number;
    name: string;
    homepage: string;
    enabled: boolean;
    logo_path: string;
    created_at: string;
    updated_at: string;
    directory_name: string;
    type: string;
}


export { Country, Currency, BaseModel, Retailer };