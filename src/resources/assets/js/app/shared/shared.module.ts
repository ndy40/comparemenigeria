import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Country } from './models.model';

@NgModule({
    imports: [CommonModule]
})
export class SharedModule {
    country: Country;
}