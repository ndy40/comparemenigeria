<!DOCTYPE html>
<html>
    
    <head>
        <base href='/'/>
        <title>NAOMI Admin Dashbaord</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.2.3/css/bulma.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    </head>
    <body
        @yield("content")
        
        <script src="{{asset("dist/app.bundle.js")}}"></script>
        @section("scripts")
        @show
    </body>
</html>
