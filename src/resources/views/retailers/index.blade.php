
<!-- **
 * Created by PhpStorm.
 * User: ndy40
 * Date: 25/03/16
 * Time: 09:07
 */
 -->

@extends("layout.master")

@section("content")

        <form class="col s12">
            <fieldset>
                <legend><span class="section-header">Add Retailer</span></legend>
                <div class="row">
                    <div class="input-field col s4">
                        <input  id="retailerName" placeholder="Retailer Name" type="text" class="validate" required title="Enter the name of this retailer" name="retailerName"/>
                    </div>
                    <div class="input-field col s5">
                        <input type="text" id="homePage" placeholder="Home Page" class="validate" required title="Enter web page of retailer"/>
                    </div>
                    <div class="input-field col s3">
                        <select name="country" id="country" required>
                            <option selected disabled value="">Select Country</option>
                            <option value="1">Nigeria</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <select>
                            <option selected disabled>Scrape Type</option>
                            <option value="scrape">Screen Scrape</option>
                            <option value="feed">Data Feed</option>
                        </select>
                    </div>
                    <div class="input-field file-field col s4">
                        <div class="btn">
                            <span>Logo</span>
                            <input type="file" name="logo" id="logo"/>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="input-field col s2">
                        <button type="submit" class="btn">Save</button>
                    </div>
                    <div class="input-field col s2">
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </div>
            </fieldset>
        </form>
    <div class="row">
        <div class="col s12">
            @if ($retailers)
                <h5>Retailers</h5>
                <table>
                    <thead>
                        <tr>
                            <th data-field="id">Logo</th>
                            <th data-field="name">Name</th>
                            <th data-field="enable">Status</th>
                            <th data-field="country_id">Country</th>
                            <td data-field="type">Job Type</td>
                            <td data-field="directory">Agent Dir.</td>
                            <td data-field="home_page">Seed Url</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($retailers as $retailer)
                            <tr>
                                <td>{{ $retailer->logo_path }}</td>
                                <td>{{ $retailer->name }}</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            Off
                                            <input type="checkbox" name="enable" checked="{{ (bool) $retailer->enabled }}"/>
                                            <span class="lever"></span>
                                            On
                                        </label>
                                    </div>
                                </td>
                                <td>{{ $retailer->country->code }}</td>
                                <td>{{ $retailer->type }}</td>
                                <td>{{ $retailer->directory_name }}</td>
                                <td>{{ $retailer->homepage }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>


    </div>
@endsection

@section("extra-scripts")
    <script type="text/javascript">

    </script>
@endsection

@section("sidebar")
    @parent
    @include("navigation.sidenav")
@endsection
