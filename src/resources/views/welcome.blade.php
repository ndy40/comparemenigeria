@extends("layout.master")

@section("content")
    <h5 class="center-align">Available Products</h5>
    <table class="bordered highlight">
        <thead>
            <tr>
                <th>ID</th>
                <th>Retailer</th>
                <th>Product Name</th>
                <th>Last Updated</th>
                <th>Price</th>
                <th>Previous Price</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="product in products">
                <td>@{{ product.id }}</td>
                <td>@{{ product.retailer.name }}</td>
                <td><a href='#'>@{{ product.name }}</a></td>
                <td>@{{ product.updated_at}}</td>
                <td>@{{ product.price }}</td>
                <td>@{{ product.price_was }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6">
                    <ul class="pagination">
                        <li class="teal lighten-2"><a href="#">1</a></li>
                        <li class="teal lighten-2"><a href="#">2</a></li>
                        <li class="teal lighten-2"><a href="#">3</a></li>
                    </ul>
                </td>
            </tr>
        </tfoot>
    </table>
@endsection

@section("right-panel")
    <fieldset><legend><strong>Filter</strong></legend>
        <div class="row">
            <div class="col input-field">
                <input type="date" placeholder="Start Date"  class="datepicker" id="startDate" name="startDate"/>
                <input type="date" placeholder="End Date"    class="datepicker" id="endDate"   name="endDate"/>
                <select>
                    <option selected="selected">All Retailers</option>
                    <option value="1">Konga.com</option>
                    <option value="2">Jumia.com</option>
                </select>

                <select>
                    <option selected disabled>Sort By</option>
                    <option>Highest - Low Price</option>
                    <option>Newest - Oldest</option>
                </select>
            </div>
            <div class="col input-field">
                <button class="waves-effect waves-light btn">Filter</button>
            </div>
        </div>
    </fieldset>
@endsection

@section("sidebar")
    @parent
    @include("navigation.sidenav")
@endsection


@section("extra-scripts")
    <script type="text/javascript" src="{{asset("module/product/products.js")}}"></script>
@endsection
