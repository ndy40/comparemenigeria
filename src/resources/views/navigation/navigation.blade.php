<nav class="nav" style="background-color: #e5f0f9;">
    <div class="nav-left">
        
        <a class="nav-item is-brand" href='#'>
            <span class="icon">
                    <i class="fa fa-home" aria-hidden></i>
            </span>
        </a>
        <a class="nav-item" href='#'>
            <span class="icon">
                <i class="fa fa-product-hunt"></i>
            </span>
            Product
        </a>
        <a class='nav-item' href='#'>
            Retailer
        </a>
        <a class='nav-item' href='#'>
            Queue
        </a>
        <a class="nav-item" href="#">
            Search Engine
        </a>
    </div>
</nav>