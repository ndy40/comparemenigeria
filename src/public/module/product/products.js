/**
 * Created by ndy40 on 21/05/16.
 */

var ProductModule = new Vue({
    el : "#main-content",
    data : {
        products : []
    },
    created : function () {
        this.products = [
            {
                "id" : 1,
                "name" : "Product 1",
                "retailer" : {"name" : "Konga.com"},
                "price" : "1.00",
                "price_was" : "0.00",
                "sku" : "sadfad",
                "updated_at" : "2012/1/2"
            },
            {
                "id" : 1,
                "name" : "Product 1",
                "retailer" : {"name" : "Konga.com"},
                "price" : "1.00",
                "price_was" : "0.00",
                "sku" : "sadfad",
                "updated_at" : "2012/1/2"
            },{
                "id" : 1,
                "name" : "Product 1",
                "retailer" : {"name" : "Konga.com"},
                "price" : "1.00",
                "price_was" : "0.00",
                "sku" : "sadfad",
                "updated_at" : "2012/1/2"
            },{
                "id" : 1,
                "name" : "Product 1",
                "retailer" : {"name" : "Konga.com"},
                "price" : "1.00",
                "price_was" : "0.00",
                "sku" : "sadfad",
                "updated_at" : "2012/1/2"
            },
        ];
    }
});
